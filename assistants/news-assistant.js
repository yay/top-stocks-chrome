function NewsAssistant() {
    
}

NewsAssistant.setup = function() {
    var self = this,
        lastFeedIndex = TopStocks.lastFeedIndex,
        newsProviderIndex = TopStocks.newsProviderIndex;
        
    this.feedListRow = loadFromFile('views/news/feedListRow.html');
    this.feedListScroller = new iScroll('feedListScroller');
    
    this.newsListRow = loadFromFile('views/news/newsListRow.html');
    this.newsListScroller = new iScroll('newsListScroller');
    
    // turns out several URLs can be separated by an asterisk
    // Google Chrome handles them correctly, but Playbook does not
    function parseFeedUrl(url) {
        var links = url.split('*');
        return links.length > 0 ? links[links.length - 1] : ''; // get the last URL
    }
    
    this.onNewsListItemClick = function(li) {
        TopStocks.nativeOpenUrl(parseFeedUrl(Quotes.newsFeed.stories[li.index()].url));
    };
    
    this.feedListClickHandler = createStickyListClickHandler('feedList',
        this.onFeedListItemClick.bind(this));
    this.newsListClickHandler = createListClickHandler('newsList',
        this.onNewsListItemClick);
        
    $('#provider').on('click', 'li', this.onProviderButtonClick.bind(this));
    
    this.updateProviderButtons();
    this.updateFeedList(function() {
        self.feedListScroller.scrollToElement('li:nth-child(' + lastFeedIndex + ')', 0);
    });
    
    setTimeout(function() {
        if (self.newsListScroller && // still on this tab
            lastFeedIndex === TopStocks.lastFeedIndex && // user hasn't already changed feed
            newsProviderIndex === TopStocks.newsProviderIndex) // or provider
        {
            self.showFeedIndex(lastFeedIndex);
            $('#feedList li:eq(' + lastFeedIndex + ')').addClass('pressed');
        }
    }, 750);
    
    Quotes.registerAssistant('news', this);
};

NewsAssistant.cleanup = function() {
    Quotes.removeAssistant('news');
    
    $('#provider').off('click');
    
    this.feedListScroller.destroy();
    delete this.feedListScroller;
    
    this.newsListScroller.destroy();
    delete this.newsListScroller;
    
    this.feedListClickHandler.destroy();
    delete this.feedListClickHandler;
    
    this.newsListClickHandler.destroy();
    delete this.newsListClickHandler;
};

NewsAssistant.showLastFeed = function() {
    
};

NewsAssistant.updateModelCallback = function() {
    var self = this;
    regenerateList('newsList', this.newsListRow, Quotes.newsFeed.stories);
    setTimeout(function() {
        self.newsListScroller.refresh();
    }, 0);
    $('#newsListSpinner').hide();
};

NewsAssistant.updateFeedList = function(onComplete) {
    var self = this,
        newsProvider = TopStocks.newsProviders[TopStocks.newsProviderIndex];
    regenerateList('feedList', this.feedListRow, newsProvider.feeds);
    setTimeout(function() {
        self.feedListScroller.refresh();
        if (isFunction(onComplete)) onComplete();
    }, 0);
};

NewsAssistant.onFeedListItemClick = function(li) {
    this.showFeedIndex(li.index());
};

NewsAssistant.showFeedIndex = function(index) {
    var newsProvider = TopStocks.newsProviders[TopStocks.newsProviderIndex],
        self = this, feed, url;
    if (index >= newsProvider.feeds.length) return;
    feed = newsProvider.feeds[index];
    url = newsProvider.baseUrl + feed.value;
        
    $('#newsListSpinner').show();
    $('#newsList').html('');
    $('#feed').html(feed.title);
    
    setTimeout(function() {
        self.newsListScroller.refresh();
    }, 0);
        
    TopStocks.lastFeedIndex = index;
    TopStocks.saveSettings();
    Quotes.updateNewsFeed(url);
};

NewsAssistant.onProviderButtonClick = function(e) {
    var li = $(e.target), index = li.index(), self = this;
    if (index !== TopStocks.newsProviderIndex) {
        TopStocks.newsProviderIndex = index;
        TopStocks.saveSettings();
        this.updateProviderButtons();
        this.updateFeedList(function() {
            self.feedListScroller.scrollTo(0,0,0);
        });
    }
};

NewsAssistant.updateProviderButtons = function() {
    var provider = $('#provider'),
        index = TopStocks.newsProviderIndex;
    provider.find('li').removeClass('selected');
    provider.find('li:eq(' + index + ')').addClass('selected');
};