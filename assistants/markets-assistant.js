function MarketsAssistant() {
    
}

MarketsAssistant.setup = function() {
    var self = this, lastQuoteIndex;
    this.indexListRow = loadFromFile('views/markets/marketsRow.html');
    this.indexListScroller = new iScroll('indexListScroller');
    this.summaryScroller = new iScroll('summaryScroller');
    
    this.companyElement = $('#company');
//    this.alertElement = $('#alert');
    this.symbolElement = $('#symbol');
    this.lastElement = $('#last');
    this.changeElement = $('#change');
    this.openElement = $('#open');
    this.highElement = $('#high');
    this.lowElement = $('#low');
    this.volumeElement = $('#volume');
    this.hi52wElement = $('#hi52w');
    this.lo52wElement = $('#lo52w');
    this.lastTradeElement = $('#lastTrade');
    this.indexChartElement = $('#indexChart');
    
    this.chartElement = $('#stockChart');
    this.chartSpinnerElement = $('#chartSpinner');
    this.chartPeriodElement = $('#chartPeriod');
    
    this.reloadIndexChart();
    
    this.onIndexListItemClick = function(li) {
        self.showQuoteIndex(li.index());
    };
    
    this.onStoryListItemClick = function(li) {
        if (this.quote)
            TopStocks.nativeOpenUrl(this.quote.feed.stories[li.index()].url);
    };
    
    this.indexListClickHandler = createListClickHandler('indexList',
        this.onIndexListItemClick);
    this.storyListClickHandler = createListClickHandler('storyList',
        this.onStoryListItemClick.bind(this));
    
    $('#refreshIndexList').on('click', this.updateIndexes.bind(this));
    $('#chartPeriod').on('click', 'li', this.onChartPeriodChange.bind(this));
    this.chartElement.on('load', function() {
        self.chartSpinnerElement.hide();
        $(this).show();
    });
    
    if (Quotes.indexList.length > 0) {
        this.updateListModel();
        if (TopStocks.lastIndex) {
            lastQuoteIndex = Quotes.indexOfSymbol(Quotes.indexList, TopStocks.lastIndex);
            if (lastQuoteIndex >= 0) {
                this.showQuoteIndex(lastQuoteIndex);
            }
        }
    }
    
    Quotes.registerAssistant('markets', this);
};

MarketsAssistant.cleanup = function() {
    Quotes.removeAssistant('markets');
    
    $('#refreshIndexList').off('click');
    $('#chartPeriod').off('click');
    this.chartElement.off('load');
    
    this.indexListClickHandler.destroy();
    delete this.indexListClickHandler;
    
    this.storyListClickHandler.destroy();
    delete this.storyListClickHandler;
    
    this.indexListScroller.destroy();
    delete this.indexListScroller;
    
    this.summaryScroller.destroy();
    delete this.summaryScroller;
};

MarketsAssistant.updateListModel = function() {
    var self = this, miniChartTime = TopStocks.getMiniChartTime();
    regenerateList('indexList', this.indexListRow, Quotes.indexList, function(li, index) {
        var itemModel = Quotes.indexList[index];
        switch (itemModel.symbol) {
            case ".DJI":
                $(li).find('.legend').addClass('index-dot blue');
                break;
            case ".IXIC":
                $(li).find('.legend').addClass('index-dot orange');
                break;
            case ".INX":
                $(li).find('.legend').addClass('index-dot red');
                break;
        }
    });
    
    // highlight ticker change with color (green or red)
    if (TopStocks.showRecentTrend) {
        var i, item, changeColor;
        for (i = 0; i < Quotes.indexList.length; i++) {
            item = Quotes.indexList[i];
            changeColor = TopStocks.getChangeColor(item.last, item.previousLast);
            if (changeColor !== '') {
                $('#indexList li:eq(' + i + ') .quote-last').addClass(changeColor);
            }
        }
    }
    setTimeout(function() {
        self.indexListScroller.refresh();
    }, 0);
};

MarketsAssistant.updateStoryModel = function(index) {
    $('#newsSpinner').hide();
    var quote = Quotes.indexList[index];
    if (quote && quote === this.quote) {
        regenerateList('storyList',
            '<li style="padding: 8px 14px; font-size: 15px;">#{summary}</li>',
            quote.feed.stories);
        this.summaryScroller.refresh();
    }
};

MarketsAssistant.updateModelCallback = function(success) {
    if (success) {
        this.updateListModel();
    }
};

MarketsAssistant.showQuoteIndex = function(index) {
    $('#company').html('');
    $('#storyList').html('');
    if ( !(index >= 0 && Quotes.indexList.length > 0) ) {
        $('#realTime').html('');
        $('#summary').hide();
        this.summaryScroller.refresh();
        return;
    }
    this.quote = Quotes.indexList[index];
    this.setLastIndex();
    this.summaryScroller.refresh();
    this.reloadChart();
    this.updateDetails();
    $('#newsSpinner').show();
    Quotes.updateCompanyFeed(Quotes.indexList, index);
};

MarketsAssistant.updateDetails = function() {
    if (!this.quote) return;
    var quote = this.quote;
    
    this.companyElement.html(quote.name + " (" + quote.exchange + ")");
    this.symbolElement.html(quote.symbol);
    this.lastElement.html(quote.last);
    this.changeElement.html(quote.changeText);
    this.changeElement.removeClass();
    this.changeElement.addClass(quote.changeColor);
    
    this.openElement.html(fillEmpty(quote.open));
    this.highElement.html(fillEmpty(quote.high));
    this.lowElement.html(fillEmpty(quote.low));
    this.volumeElement.html(fillEmpty(quote.volume));
    this.hi52wElement.html(fillEmpty(quote.high52week));
    this.lo52wElement.html(fillEmpty(quote.low52week));
    
    this.lastTradeElement.html(quote.lastTradeDateTime);
    
    if (TopStocks.showRecentTrend) {
        this.lastElement.removeClass();
        var changeColor = TopStocks.getChangeColor(quote.last, quote.previousLast);
        if (changeColor !== '') {
            this.lastElement.addClass(changeColor);
        }
    }
    
    if (TopStocks.realTimeIndexes.indexOf(quote.exchange) >= 0) {
        $('#realTime').html('REAL<br>TIME').css('color', 'green');
    } else {
        $('#realTime').html('DELAYED<br>15-20 min').css('color', '#EB0000');
    }
    
    $('#summary').show();
};

MarketsAssistant.hideRefreshButtonForAwhile = function() {
    var refreshButton = $('#refreshIndexList');
    refreshButton.addClass('disabled');
    setTimeout(function() {
        refreshButton.removeClass('disabled');
    }, 5000);
};

MarketsAssistant.updateIndexes = function() {
    if ( !$('#refreshIndexList').hasClass('disabled') ) {
        this.hideRefreshButtonForAwhile();
        this.reloadIndexChart();
        Quotes.updateQuotes(Quotes.indexList);
    }
};

MarketsAssistant.reloadIndexChart = function() {
    this.indexChartElement.attr('src', TopStocks.indexChartUrl.interpolate({
        chartSize: "m",
        timeLineFormat: "12",
        append: "&xxx=" + (new Date()).getTime()
    }));
};

MarketsAssistant.onChartPeriodChange = function(e) {
    TopStocks.chartZoom = e.target.innerHTML;
    TopStocks.saveSettings();
    this.reloadChart();
};

MarketsAssistant.reloadChart = function() {
    if (!this.quote) return;
    this.chartPeriodElement.find('li').removeClass('selected');
    this.chartPeriodElement.find('li:contains("' + TopStocks.chartZoom + '")').addClass('selected');
    
    this.chartElement.hide();
    this.chartSpinnerElement.show();
    this.chartElement.attr('src', TopStocks.stockChartUrl.interpolate({
        symbol: this.quote.symbol,
        exchange: this.quote.exchange,
        timeLineFormat: "12",
        period: TopStocks.chartZoom,
        append: !TopStocks.cacheCharts ? "&xxx=" + (new Date()).getTime() : ""
    }));
};

MarketsAssistant.setLastIndex = function() {
    TopStocks.lastIndex = this.quote ? (this.quote.exchange + ':' + this.quote.symbol) : '';
};