function WatchlistAssistant() {
    
}

WatchlistAssistant.setup = function() {
    var self = this, lastQuoteIndex;
    this.addedSymbols = '';
    this.symbolsHint = 'Enter space-separated symbols';
    
    this.watchListRow = loadFromFile('views/watchlist/watchListRow.html');
    this.watchListScroller = new iScroll('watchListScroller');
    this.summaryScroller = new iScroll('summaryScroller');
    
    this.companyElement = $('#company');
//    this.alertElement = $('#alert');
    this.symbolElement = $('#symbol');
    this.lastElement = $('#last');
    this.lastTradeElement = $('#lastTrade');
    this.changeElement = $('#change');
    this.openElement = $('#open');
    this.highElement = $('#high');
    this.lowElement = $('#low');
    this.volumeElement = $('#volume');
    this.avgVolElement = $('#avgVol');
    this.marketCapElement = $('#marketCap');
    this.hi52wElement = $('#hi52w');
    this.lo52wElement = $('#lo52w');
    this.PEElement = $('#PE');
    this.betaElement = $('#beta');
    this.dividendElement = $('#dividend');
    this.yieldElement = $('#yield');
    
    this.extSymbolElement = $('#extSymbol');
    this.extLastElement = $('#extLast');
    this.extChangeElement = $('#extChange');
    this.extLastTradeElement = $('#extLastTrade');
    this.extendedElement = $('#extended');
    
    this.chartElement = $('#stockChart');
    this.chartSpinnerElement = $('#chartSpinner');
    this.chartPeriodElement = $('#chartPeriod');
    
    this.lookupSpinnerElement = $('#lookupSpinner');
    this.updateHintElement = $('#updateHint');
    
    this.watchListClickHandler = createListClickHandler('watchList',
        this.onWatchListItemClick.bind(this));
    this.storyListClickHandler = createListClickHandler('storyList',
        this.onStoryListItemClick.bind(this));
    
    this.refreshUpdateHint();
    
    // can't put onfocus handler into the watchlist-scene.html, as we'll get this error:
    // "Refused to execute inline event handler because of Content-Security-Policy."
    // when running as a Google Chrome extension
    $('#symbols').on('focus', function() {
        if (this.value === WatchlistAssistant.symbolsHint) this.value='';
        this.style.fontStyle='normal';
        this.style.color = '#000';
    });
    
    $('#removeSymbols').on('click', this.onRemoveSymbolsClick);
    $('#sortWatchList').on('click', this.toggleSorting.bind(this));
    $('#lookupSymbol').on('click', this.toggleLookup.bind(this));
    this.updateHintElement.on('click', this.toggleUpdates.bind(this));
    $('#refreshWatchList').on('click', this.updateQuotes.bind(this));
    $('#addSymbols').on('click', this.showAddSymbolsDialog.bind(this));
    $('#addSymbolsClose').on('click', this.hideAddSymbolsDialog.bind(this));
    $('#addSymbolsAdd').on('click', this.addSymbols.bind(this));
    $('#chartPeriod').on('click', 'li', this.onChartPeriodChange.bind(this));
    $('#closeSorting').on('click', this.toggleSorting.bind(this));
    $('#closeUpdates').on('click', this.toggleUpdates.bind(this));
    $('#closeLookup').on('click', this.toggleLookup.bind(this));
    $('#sorting').on('click', 'li', this.onSortButtonClick.bind(this));
    $('#interval').on('click', 'li', this.onIntervalButtonClick.bind(this));
    this.chartElement.on('load', function() {
        self.chartSpinnerElement.hide();
        $(this).show();
    });
    $('#symbols').on('keypress', function(e) { // allow Enter key to confirm input
        if ( (e.keyCode || e.which) == 13 ) self.addSymbols();
    });
    $('#symbols').on('input', function() {
        $(this).val($(this).val().toUpperCase());
    });
    $('#query').on('input', function() {
        var value = $(this).val();
        if (self.lookupTaskId) {
            clearTimeout(self.lookupTaskId);
        }
        self.lookupTaskId = setTimeout(function() {
            delete self.lookupTaskId;
            self.filterFunction(value);
        }, 500);
    });
    
    if (Quotes.watchList.length > 0) {
        this.updateListModel();
        if (TopStocks.lastStock) {
            lastQuoteIndex = Quotes.indexOfSymbol(Quotes.watchList, TopStocks.lastStock);
            if (lastQuoteIndex >= 0) {
                this.showQuoteIndex(lastQuoteIndex);
            }
        }
    }
    
    Quotes.registerAssistant('quoteList', this);
};

WatchlistAssistant.cleanup = function() {
    Quotes.removeAssistant('quoteList');
    
    delete this.lookupItems;
    
    $('#symbols').off('focus');
    
    $("#removeSymbols").off('click');
    $('#refreshWatchList').off('click');
    $('#addSymbols').off('click');
    this.updateHintElement.off('click');
    $('#addSymbolsClose').off('click');
    $('#addSymbolsAdd').off('click');
    $('#symbols').off('input');
    $('#query').off('input');
    $('#chartPeriod').off('click');
    $('#closeSorting').off('click');
    $('#closeLookup').off('click');
    $('#closeUpdates').off('click');
    $('#sortWatchList').off('click');
    $('#lookupSymbol').off('click');
    $('#sorting').off('click');
    $('#interval').off('click');
    this.chartElement.off('load');
    
    this.watchListScroller.destroy();
    delete this.watchListScroller;
    
    this.watchListClickHandler.destroy();
    delete this.watchListClickHandler;
    
    this.summaryScroller.destroy();
    delete this.summaryScroller;
    
    if (this.lookupListScroller) {
        this.lookupListScroller.destroy();
        delete this.lookupListScroller;
    }
    
    if (this.lookupListClickHandler) {
        this.lookupListClickHandler.destroy();
        delete this.lookupListClickHandler;
    }
    
    this.storyListClickHandler.destroy();
    delete this.storyListClickHandler;
};

WatchlistAssistant.updateDetails = function() {
    if (!this.quote) return;
    var quote = this.quote;
    
    this.companyElement.html(quote.name + " (" + quote.exchange + ")");
    this.symbolElement.html(quote.symbol);
    this.extSymbolElement.html(quote.symbol);
    this.lastElement.html(quote.last);
    this.changeElement.html(quote.changeText);
    this.changeElement.removeClass();
    this.changeElement.addClass(quote.changeColor);

    this.openElement.html(fillEmpty(quote.open));
    this.highElement.html(fillEmpty(quote.high));
    this.lowElement.html(fillEmpty(quote.low));
    this.volumeElement.html(fillEmpty(quote.volume));
    this.avgVolElement.html(fillEmpty(quote.averageVolume));
    this.marketCapElement.html(fillEmpty(quote.marketCap));
    this.hi52wElement.html(fillEmpty(quote.high52week));
    this.lo52wElement.html(fillEmpty(quote.low52week));
    this.PEElement.html(fillEmpty(quote.PE));
    this.betaElement.html(fillEmpty(quote.beta));
    this.dividendElement.html(fillEmpty(quote.dividend));
    this.yieldElement.html(fillEmpty(quote.yield));

    this.lastTradeElement.html(quote.lastTradeDateTime);
    if (quote.extLast) {
        this.extendedElement.show();

        this.extLastElement.html(quote.extLast);
        this.extChangeElement.html(quote.extChangeText);
        this.extChangeElement.removeClass();
        this.extChangeElement.addClass(quote.extChangeColor);
        this.extLastTradeElement.html(quote.extLastTradeDateTime);
    } else {
        this.extendedElement.hide();
    }

    if (TopStocks.showRecentTrend) {
        this.lastElement.removeClass();
        var changeColor = TopStocks.getChangeColor(quote.last, quote.previousLast);
        if (changeColor !== '') {
            this.lastElement.addClass(changeColor);
        }
    }
    
    if (TopStocks.realTimeExchanges.indexOf(quote.exchange) >= 0) {
        $('#realTime').html('REAL<br>TIME').css('color', 'green');
    } else {
        $('#realTime').html('DELAYED<br>15-20 min').css('color', '#EB0000');
    }
    
    $('#summary').show();
};

WatchlistAssistant.updateModelCallback = function(success) {
    if (success) {
        this.updateListModel();
    }
};

WatchlistAssistant.updateStoryModel = function(index) {
    $('#newsSpinner').hide();
    var quote = Quotes.watchList[index];
    if (quote && quote === this.quote) {
        regenerateList('storyList',
            '<li style="padding: 8px 14px; font-size: 15px;">#{summary}</li>',
            quote.feed.stories);
        this.summaryScroller.refresh();
    }
};

WatchlistAssistant.updateListModel = function() {
    var self = this;
    
    if (Number(TopStocks.sortMethod) > 0) {
        Quotes.sortWatchList();
    }
    
    regenerateList('watchList', this.watchListRow, Quotes.watchList);
    // remove after hours quotes if empty
    $('#watchList .last:empty').parent().remove();
    // highlight ticker change with color (green or red)
    if (TopStocks.showRecentTrend) {
        var i, item, changeColor;
        for (i = 0; i < Quotes.watchList.length; i++) {
            item = Quotes.watchList[i];
            changeColor = TopStocks.getChangeColor(item.last, item.previousLast);
            if (changeColor !== '') {
                $('#watchList li:eq(' + i + ') .quote-last').addClass(changeColor);
            }
        }
    }
    setTimeout(function() {
        self.watchListScroller.refresh();
    }, 0);
    
    this.updateDetails();
    this.updateLastUpdatedLabel();
};

WatchlistAssistant.updateLastUpdatedLabel = function() {
    if (Quotes.watchListLastUpdateTime > 0) {
        var d = new Date();
        d.setTime(Quotes.watchListLastUpdateTime);

        $("#lastUpdated").html('Last updated: ' +
            this.hmsToStr(d.getHours(), d.getMinutes(), d.getSeconds())
        );
    }
};

WatchlistAssistant.onWatchListItemClick = function(li) {
    var self = this, index = li.index(), removeListItem = function() {
        li.off('webkitAnimationEnd', removeListItem);
        if (Quotes.watchList[index] === self.quote) {
            delete self.quote;
            self.setLastStock();
            self.showQuoteIndex(-1);
        }
        Quotes.watchList.splice(index, 1);
        Quotes.dbStoreWatchList();
        li.remove();
        self.watchListScroller.refresh();
    };
    if ($("#removeSymbols").hasClass('selected')) {
        li.addClass('removed');
        li.on('webkitAnimationEnd', removeListItem);
    } else {
        self.showQuoteIndex(index);
    }
};

WatchlistAssistant.showQuoteIndex = function(index) {
    $('#company').html('');
    $('#storyList').html('');
    if ( !(index >= 0 && Quotes.watchList.length > 0) ) {
        $('#realTime').html('');
        $('#summary').hide();
        this.summaryScroller.refresh();
        return;
    }
    this.quote = Quotes.watchList[index];
    this.setLastStock();
    this.summaryScroller.refresh();
    this.reloadChart();
    this.updateDetails();
    $('#newsSpinner').show();
    Quotes.updateCompanyFeed(Quotes.watchList, index);
};

WatchlistAssistant.setLastStock = function() {
    TopStocks.lastStock = this.quote ? (this.quote.exchange + ':' + this.quote.symbol) : '';
};

WatchlistAssistant.onLookupListItemClick = function(li) {
    var self = this, query = $('#query'), item, symbol, index;
    if (!this.lookupItems) return;
    
    item = this.lookupItems[li.index()];
    symbol = item.e + ':' + item.t;
    index = Quotes.indexOfSymbol(Quotes.watchList, symbol);
    
    function scrollToElement() {
        if (self.watchListScroller) { // if we are still on the Watchlist tab
            self.watchListScroller.scrollToElement('li:nth-child(' +
                    parseInt(index + 1, 10) + ')', 750);
        }
    }
    
    function hideLookup() {
        if (self.watchListScroller) { // if we are still on the Watchlist tab
            self.toggleLookup();
            if (index >= 0) {
                setTimeout(scrollToElement, 600);
            }
        }
    }
    
    if (query.is(':focus')) {
        query.blur(); // remove focus from the input to hide the keyboard
        setTimeout(hideLookup, 1000);
    } else {
        hideLookup();
    }
    
    if (index < 0) {
        Quotes.addQuotes(symbol);
    } else {
        console.info('The symbol is already in the watchlist.');
    }
};

WatchlistAssistant.onStoryListItemClick = function(li) {
    if (this.quote)
        TopStocks.nativeOpenUrl(this.quote.feed.stories[li.index()].url);
};

WatchlistAssistant.onRemoveSymbolsClick = function() {
    if ($("#removeSymbols").hasClass('selected')) {
        $('#watchList').removeClass('edit');
        $("#removeSymbols").removeClass('selected');
    } else {
        $("#removeSymbols").addClass('selected');
        $('#watchList').addClass('edit');
    }
};

WatchlistAssistant.hmsToStr = function(h, m, s) {
    function makeDouble(digit) {
        var str = String(digit);
        if (str.length === 1) {
            return "0" + str;
        } else {
            return str;
        }
    }

    var period = h < 12 ? "AM" : "PM",
        second = makeDouble(s),
        minute = makeDouble(m),
        hour = String(h > 12 ? h - 12 : h);

    return hour + ":" + minute + ":" + second + " " + period + " " + TopStocks.timeZone;
};

WatchlistAssistant.showQuoteError = function() {
    var msg, msgTemplate = "Quote#{s} not found. #{symbols} do#{es} not match any company symbol, or there was a problem connecting to the server.";
    if (this.addedSymbols.count(",")) {
        msg = msgTemplate.interpolate({s: "s", symbols: this.addedSymbols, es: ""});
    } else {
        msg = msgTemplate.interpolate({s: "", symbols: this.addedSymbols, es: "es"});
    }
    TopStocks.nativeAlert(msg);
};

WatchlistAssistant.hideRefreshButtonForAwhile = function() {
    var refreshButton = $('#refreshWatchList');
    refreshButton.addClass('disabled');
    setTimeout(function() {
        refreshButton.removeClass('disabled');
    }, 5000);
};

WatchlistAssistant.updateQuotes = function() {
    if ( !$('#refreshWatchList').hasClass('disabled') ) {
        this.hideRefreshButtonForAwhile();
        Quotes.updateQuotes(Quotes.watchList);
    }
};

WatchlistAssistant.updateModelCallback = function(success) {
    if (success)
        this.updateListModel();
//    if (success) {
//        if (this.quoteListDragging || this.quoteListHolding) {
//            // cannot update right now, delay update
//            this.delayListUpdate = true;
//        } else {
//            this.updateListModel();
//        }
//    }
};

WatchlistAssistant.showAddSymbolsDialog = function() {
    $('#symbols').val(this.symbolsHint)[0].removeAttribute('style');
    $('#addSymbolsFader').show().animate({opacity: 1}, 400);
    $('#addSymbolsDialog').show().animate({bottom: 0}, 400);
};

WatchlistAssistant.hideAddSymbolsDialog = function() {
    $('#addSymbolsFader').animate({opacity: 0}, 400, function() {
        $(this).hide();
    });
    $('#addSymbolsDialog').animate({bottom: -200}, 400, function(){
        $('#symbols').val(self.symbolsHint)[0].removeAttribute('style');
        $(this).hide();
    });
};

WatchlistAssistant.addSymbols = function() {
    var symbols = $('#symbols').blur().val();
    this.hideAddSymbolsDialog();
    if (symbols !== '' && symbols !== this.symbolsHint) {
        symbols = Quotes.checkSymbols(symbols);
        if (symbols !== '') {
            this.addedSymbols = symbols;
            Quotes.addQuotes(symbols);
        }
    }
};

WatchlistAssistant.onChartPeriodChange = function(e) {
    TopStocks.chartZoom = e.target.innerHTML;
    TopStocks.saveSettings();
    this.reloadChart();
};

WatchlistAssistant.reloadChart = function() {
    if (!this.quote) return;
    this.chartPeriodElement.find('li').removeClass('selected');
    this.chartPeriodElement.find('li:contains("' + TopStocks.chartZoom + '")').addClass('selected');
    
    this.chartElement.hide();
    this.chartSpinnerElement.show();
    this.chartElement.attr('src', TopStocks.stockChartUrl.interpolate({
        symbol: this.quote.symbol,
        exchange: this.quote.exchange,
        timeLineFormat: "12",
        period: TopStocks.chartZoom,
        append: !TopStocks.cacheCharts ? "&xxx=" + (new Date()).getTime() : ""
    }));
};

WatchlistAssistant.toggleSorting = function() {
    var sorting = $('#sorting'), self = this,
        hideSorting = function() {
            sorting.off('webkitAnimationEnd', hideSorting);
            sorting.removeClass('hide');
            sorting.hide();
        };
    if (sorting.is(':visible')) {
        TopStocks.saveSettings();
        // sort the watchlist
        if (Number(TopStocks.sortMethod) > 0) {
            Quotes.sortWatchList();
            regenerateList('watchList', this.watchListRow, Quotes.watchList);
            // remove after hours quotes if empty
            $('#watchList .last:empty').parent().remove();
            setTimeout(function() {
                self.watchListScroller.refresh();
            }, 0);
        }
        // hide the dialog
        $('#sortWatchList').removeClass('selected');
        sorting.addClass('hide');
        sorting.on('webkitAnimationEnd', hideSorting);
    } else {
        this.updateSortButtons();
        $('#sortWatchList').addClass('selected');
        sorting.show();
    }
};

WatchlistAssistant.toggleLookup = function() {
    var lookup = $('#lookup'), self = this,
        hideLookup = function() {
            lookup.hide();
            $('#lookupList').html('');
            if (self.lookupListScroller) {
                self.lookupListScroller.refresh();
            }
            $('#query').val('');
        },
        onHideEnd = function() {
            lookup.off('webkitAnimationEnd', onHideEnd);
            lookup.removeClass('hide');
            hideLookup();
        };
    if (lookup.is(':visible')) {
        $('#lookupSymbol').removeClass('selected'); // unpress the lookup button
        lookup.addClass('hide');
        lookup.on('webkitAnimationEnd', onHideEnd);
    } else {
        if (!this.lookupListScroller) {
            this.lookupListScroller = new iScroll('lookupListScroller');
        }
        if (!this.lookupListRow) {
            this.lookupListRow = loadFromFile('views/watchlist/lookupListRow.html');
        }
        if (!this.lookupListClickHandler) {
            this.lookupListClickHandler = createListClickHandler('lookupList',
                this.onLookupListItemClick.bind(this));
        }
        $('#lookupSymbol').addClass('selected');
        lookup.show();
        setTimeout(function(){
            $('#query').focus();
        }, 1000);
    }
};

WatchlistAssistant.onSortButtonClick = function(e) {
    var li = $(e.target), index = li.index() - 1;
    if (index >= 0) {
        if (li.parent()[0].id === 'sortMethod') {
            TopStocks.sortMethod = index;
        } else { // id === 'sortOrder'
            TopStocks.sortOrder = index;
        }
        this.updateSortButtons();
    }
};

WatchlistAssistant.toggleUpdates = function() {
    var updates = $('#updates'),
        hideUpdates = function() {
            updates.off('webkitAnimationEnd', hideUpdates);
            updates.removeClass('hide');
            updates.hide();
        };
    if (updates.is(':visible')) {
        clearTimeout(TopStocks.updateTaskId);
        TopStocks.scheduleQuotesUpdate();
        TopStocks.saveSettings();
//        console.info('Update interval changed to', TopStocks.updateInterval / 1000, 'seconds.');
        this.refreshUpdateHint();
        updates.addClass('hide');
        updates.on('webkitAnimationEnd', hideUpdates);
    } else {
        this.updateIntervalButtons();
        updates.show();
    }
};

WatchlistAssistant.onIntervalButtonClick = function(e) {
    var li = $(e.target), index = li.index() - 1;
    if (index >= 0) {
        TopStocks.updateInterval = TopStocks.updateIntervals[index];
        this.updateIntervalButtons();
    }
};

WatchlistAssistant.updateIntervalButtons = function() {
    var interval = $('#interval'),
        index = TopStocks.updateIntervals.indexOf(TopStocks.updateInterval) + 1;
    if (index > 0) {
        interval.find('li').removeClass('selected');
        interval.find('li:eq(' + index + ')').addClass('selected');
    }
};

WatchlistAssistant.refreshUpdateHint = function() {
    if (TopStocks.updateInterval > 0) {
        this.updateHintElement.html('AUTO<br>UPDATES').css('color', 'green');
    } else {
        this.updateHintElement.html('MANUAL<br>UPDATES').css('color', '#EB0000');
    }
};

WatchlistAssistant.updateSortButtons = function() {
    var sm = Number(TopStocks.sortMethod),
        so = Number(TopStocks.sortOrder),
        sme = $('#sortMethod'),
        soe = $('#sortOrder');
    sme.find('li').removeClass('selected');
    sme.find('li:eq(' + (sm + 1) + ')').addClass('selected');
    soe.find('li').removeClass('selected');
    if (sm > 0) {
        soe.find('li:eq(' + (so + 1) + ')').addClass('selected');
    }
};

WatchlistAssistant.filterFunction = function(filterString) {
    var items = [], self = this;
    // if lookup string is null, return empty list, otherwise build results list
    if (filterString !== '') {
        self.lookupSpinnerElement.show();
        Quotes.lookupSymbol(filterString,
            function(transport) {
                var data;
                try {
                    data = JSON.parse(unescape(transport.responseText.replace(/\\x/g, '%')));
                } catch (e) {
                    console.error('JSON.parse error on stock lookup: ', e.message);
                    return;
                }
                for (var i = 0; i < data.matches.length; i++) {
                    if (data.matches[i].t !== '') {
                        items.push(data.matches[i]);
                    }
                }
                regenerateList('lookupList', self.lookupListRow, items);
                self.lookupItems = items;
                self.lookupListScroller.refresh();
                self.lookupSpinnerElement.hide();
            },
            function(transport) {
                self.lookupSpinnerElement.hide();
                console.info("Lookup failed:", transport.responseText);
            }
        );
    } else {
        $('#lookupList').html('');
        self.lookupListScroller.refresh();
        self.lookupSpinnerElement.hide();
    }
};
