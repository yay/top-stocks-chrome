var topStocksTabId;

// TODO: move quotes.js here if adding alerts feature to the app

// Fired when a browser action icon is clicked.
// This event will not fire if the browser action has a popup.
chrome.browserAction.onClicked.addListener(function() {
    if (!topStocksTabId) {
        // http://code.google.com/chrome/extensions/tabs.html
        chrome.tabs.create({'url': chrome.extension.getURL('index.html')}, function(tab) {
            // Tab opened. Remember tab ID.
            topStocksTabId = tab.id;
        });
    } else {
        // Retrieves details about the specified tab.
        chrome.tabs.get(topStocksTabId, function(tab) {
            if (tab && !tab.selected) {
                // Modifies the properties of a tab. Making tab active in this case.
                chrome.tabs.update(topStocksTabId, {active: true}, function(tab) {
                });
            }
        });
    }
});

// Fired when a tab is closed.
chrome.tabs.onRemoved.addListener(function(tabId, removeInfo) {
    if (tabId === topStocksTabId) {
        topStocksTabId = undefined;
    }
});

// A selection menu item onclick callback function.
//function selectionOnClick(info, tab) {
//    alert(JSON.stringify(info));
//    alert(JSON.stringify(tab));
//    alert(info.selectionText);
//    tab.window.close();
//}

// create a menu item for the 'selection' context
// ("contextMenus" persmission required in manifest.json)
//chrome.contextMenus.create({title: "Add to Watchlist",
//                            contexts: ['selection'],
//                            onclick: selectionOnClick});