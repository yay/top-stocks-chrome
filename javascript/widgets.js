function regenerateList(id, row, data, onItemRendered, onItemProcessed) {
    var i, li, ul = document.getElementById(id), html = '';
    if (ul === null) return;
    ul.innerHTML = '';
    if (onItemRendered) {
        for (i = 0; i < data.length; i++) {
            li = document.createElement('li');
            li.innerHTML = row.interpolate(data[i]);
            ul.appendChild(li);
            onItemRendered(li, i);
        }
    } else {
        for (i = 0; i < data.length; i++) {
            if (onItemProcessed) onItemProcessed(data[i]);
            html += row.interpolate(data[i]);
        }
        ul.innerHTML = html;
    }
}

function createListClickHandler(listId, onClick) {
    var list = document.getElementById(listId),
        event = isDevice ? 'touchstart' : 'mousedown',
        pressed = 'pressed',
        clickHandler = function() {
            var li = $(this), moveCount = 0, pressTaskId;
            if (li.hasClass('static')) return;
            
            function cancelPress() {
                if (pressTaskId) { // list item doesn't yet have a pressed look
                    clearTimeout(pressTaskId); // cancel setting it
                    pressTaskId = undefined;
                } else { // list item already has a pressed look
                    moveCount++;
                    if (moveCount > 5)
                        li.removeClass(pressed); // remove it
                }
            }
            function unpress() {
                if (isDevice) {
                    $(document).off('touchend', unpress);
                    $(document).off('touchmove', cancelPress);
                } else {
                    $(document).off('mouseup', unpress);
                    $(document).off('mousemove', cancelPress);
                }
                cancelPress();
                if (li.hasClass(pressed)) {
                    var parent = li.parent()[0];
                    if (parent._onClick && jQuery.isFunction(parent._onClick))
                        parent._onClick(li);
                    setTimeout(function() {
                        li.removeClass(pressed);
                    }, 200);
                }
            }
                
            pressTaskId = setTimeout(function() {
                li.addClass(pressed);
            }, 50);
                
            if (isDevice) {
                $(document).on('touchmove', cancelPress);
                $(document).on('touchend', unpress);
            } else {
                $(document).on('mousemove', cancelPress);
                $(document).on('mouseup', unpress);
            }
        };
    if (!list) return null;
    list._onClick = onClick;
    $(list).on(event, 'li', clickHandler);
    return {
        destroy: function() {
            $(list).off(event, 'li', clickHandler);
            delete list._onClick;
        }
    }
}

function createStickyListClickHandler(listId, onClick) {
    var list = document.getElementById(listId),
        event = isDevice ? 'touchstart' : 'mousedown',
        pressed = 'pressed',
        clickHandler = function() {
            var li = $(this), moveCount = 0, pressTaskId;
            if (li.hasClass('static')) return;
            
            function cancelPress() {
                if (pressTaskId) { // list item doesn't yet have a pressed look
                    clearTimeout(pressTaskId); // cancel setting it
                    pressTaskId = undefined;
                } else { // list item already has a pressed look
                    moveCount++;
                    if (moveCount > 10)
                        li.removeClass(pressed); // remove it
                }
            }
            function unpress() {
                if (isDevice) {
                    $(document).off('touchend', unpress);
                    $(document).off('touchmove', cancelPress);
                } else {
                    $(document).off('mouseup', unpress);
                    $(document).off('mousemove', cancelPress);
                }
                cancelPress();
                if (li.hasClass(pressed)) {
                    var parent = li.parent()[0];
                    if (parent._onClick && jQuery.isFunction(parent._onClick))
                        parent._onClick(li);
                }
            }
            
            pressTaskId = setTimeout(function() {
                li.parent().find('li').removeClass(pressed);
                li.addClass(pressed);
            }, 50);
            
            if (isDevice) {
                $(document).on('touchmove', cancelPress);
                $(document).on('touchend', unpress);
            } else {
                $(document).on('mousemove', cancelPress);
                $(document).on('mouseup', unpress);
            }
        };
    if (!list) return null;
    list._onClick = onClick;
    $(list).on(event, 'li', clickHandler);
    return {
        destroy: function() {
            $(list).off(event, 'li', clickHandler);
            delete list._onClick;
        }
    }
}

//function setListItemOnClick(id, state, onClick) {
//    $('#' + id)[state]('mousedown', 'li', function() {
//        var li = $(this),
//            press = setTimeout(function() {
//                li.addClass('pressed');
//            }, 50),
//            cancelPress = function() {
//                clearTimeout(press);
//            },
//            unpress = function() {
//                $(document).off('mouseup', unpress);
//                $(document).off('mousemove', cancelPress);
//                cancelPress();
//                if (li.hasClass('pressed')) {
//                    onClick();
//                    setTimeout(function() {
//                        li.removeClass('pressed');
//                    }, 200);
//                }
//            };
//        $(document).on('mousemove', cancelPress);
//        $(document).on('mouseup', unpress);
//    });
//}