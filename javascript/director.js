function loadFromFile(fileName) {
    var result;
    $.ajax(fileName, {
        async: false,
        dataType: 'html',
        success: function(data) {
            result = data;
        },
        error: function() {
            result = null;
        }
    });
    return result;
}

var Director = {
    stageId: 'main_stage',
    currentSceneName: '',
    currentAssistantName: '',
    _canChangeScene: true,
    canChangeScene: function(sceneName) {
        return sceneName !== this.currentSceneName && this._canChangeScene;
    },
    changeScene: function(sceneName) {
        var self = this;
//        if (!this.canChangeScene(sceneName)) return false;
        this.cleanupAssistant();
        this.cleanupScene();
        this.setupScene(sceneName);
        this.currentSceneName = sceneName;
        this.currentAssistantName = this.getAssistantName(sceneName);
        this.setupAssistant();
        this._canChangeScene = false;
        // prevent too fast scene changes
        setTimeout(function() {
            if (self) self._canChangeScene = true;
        }, 750);
//        return true;
    },
    setupScene: function(sceneName) {
        document.getElementById(this.stageId).innerHTML =
            loadFromFile('views/' + sceneName + '/' + sceneName + '-scene.html');
    },
    cleanupScene: function() {
        document.getElementById(this.stageId).innerHTML = '';
        this.currentSceneName = '';
    },
    getAssistantName: function(sceneName) {
        return sceneName.charAt(0).toUpperCase() + sceneName.slice(1) + 'Assistant';
    },
    setupAssistant: function() {
        if (!this.currentAssistantName) return;
        window[this.currentAssistantName].setup();
    },
    cleanupAssistant: function() {
        if (!this.currentAssistantName) return;
        window[this.currentAssistantName].cleanup();
        this.currentAssistantName = '';
    }
};

window.addEventListener('load', function() {
});