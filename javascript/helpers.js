function isFunction(obj) {
    return typeof obj === 'function';
}

Object.extend = function (destination, source) {
    for (var property in source)
        destination[property] = source[property];
    return destination;
};

Function.prototype.bind = function (context) {
    var slice = Array.prototype.slice;
    function update(array, args) {
        var arrayLength = array.length, length = args.length;
        while (length--) array[arrayLength + length] = args[length];
        return array;
    }
    function merge(array, args) {
        array = slice.call(array, 0);
        return update(array, args);
    }
    if (arguments.length < 2 && typeof arguments[0] === 'undefined') return this;
    var __method = this, args = slice.call(arguments, 1);
    return function() {
        var a = merge(args, arguments);
        return __method.apply(context, a);
    }
};

// unique() - eliminates duplicates
Array.prototype.unique = function() {
    var i,
        len = this.length,
        out = [],
        obj = {};

    for (i = 0; i < len; i++) {
        obj[this[i]] = 0;
    }
    for (i in obj) {
        out.push(i);
    }
    return out;
};

// alternative method (probably not as fast)
// myArray.filter(function(o, i, a) { return a.indexOf(o) === i; });

// count(ss) - counts the number of occurrences of a specific substring in a string
String.prototype.count = function(ss) {
    return (this.length - this.replace(new RegExp(ss, 'g'), '').length) / ss.length;
};

String.prototype.interpolate = function(object) {
    var pattern = /(#\{(.*?)\})/g;
    return this.replace(pattern, function() {
        var name = arguments[2];
        return typeof object[name] === 'string' ? object[name] : '';
    });
};

// checks if the string is either empty or containing only whitespace 
String.prototype.blank = function() {
    return /^\s*$/.test(this);
};

// decodeHTML() - converts the entity forms of special HTML characters to their normal form
//String.prototype.decodeHTML = function() {
//    var div = document.createElement("div");
//    div.innerHTML = this;
//    return div.childNodes[0].nodeValue;
//};

// removeStyle(str, styles) - removes all unwanted "styles" from HTML tags in "str"
function removeStyle(str, styles) {
    return str.replace(/<([^>]*)style="([^"]+)"([^>]*)>/ig, function() {
        var i, j, keep = arguments[2].split(';');
        for (i = keep.length - 1; i >= 0; i--) {
            j = styles.indexOf(keep[i].split(':')[0]);
            if (j !== -1) {
                keep.splice(i, 1);
            }
        }
        if (keep.length > 0) {
            return '<' + arguments[1] + 'style="' + keep.join(';') + '"' +
                arguments[3] + '>';
        } else {
            return '<' + arguments[1] + arguments[3] + '>';
        }
    });
}

// removeStyle(str, styles) - keeps only "styles" in HTML tags in "str"
function keepOnlyStyle(str, styles) {
    return str.replace(/<([^>]*)style="([^"]+)"([^>]*)>/ig, function() {
        var i, have = arguments[2].split(';'), keep = [];
        for (i = have.length - 1; i >= 0; i--) {
            if (styles.indexOf(have[i].split(':')[0]) !== -1) {
                keep.push(have[i]);
            }
        }
        if (keep.length > 0) {
            return '<' + arguments[1] + 'style="' + keep.join(';') + '"' +
                arguments[3] + '>';
        } else {
            return '<' + arguments[1] + arguments[3] + '>';
        }
    });
}

function toCurrency(value, currencyCode) {
    var number = typeof value == 'string' ? parseFloat(value) : value,
        digits = number.toFixed(2).split('.'),
        //digits = (typeof value == 'string' ? parseFloat(value) : value).toFixed(2).split('.'),
        negative = number < 0,
        symbol = '';
    if (currencyCode) {
        switch (currencyCode) {
            case 'USD':
                symbol = '$';
                break;
            case 'EUR':
                symbol = '€';
                break;
            case 'GBP':
                symbol = '£';
                break;
            case 'JPY':
            case 'CNY':
                symbol = '¥';
                break;
            default:
                symbol = currencyCode;
                break;
        }
    }
    digits[0] = digits[0].split('').reverse().join('').replace(/(\d{3})(?=\d)/g, '$1,').split('').reverse().join('');
    return symbol.length > 1 ? digits.join('.') + ' ' + symbol : (negative ? digits.join('.').replace(/^-/, '-' + symbol) : symbol + digits.join('.'));
}

// fillEmpty(str, value) - if string "str" is empty fills it with "-" or optional "value"
function fillEmpty(str, value) {
    return (typeof str != 'string' || str == '') ? (value ? value : '-') : str;
}

function xhr_get(url, params, onSuccess, onFailure, headers) {
    var request = new XMLHttpRequest(),
        name, paramArray = [], paramString = '', xhrTimeout;
    function onTimeout() {
        request.abort();
        if (isFunction(onFailure)) {
            onFailure(request);
        }
    }
    if (params) {
        for (name in params) {
            paramArray[paramArray.length] = name + '=' + encodeURIComponent(params[name]);
        }
        paramString = paramArray.join('&');
    }
    request.onreadystatechange = function() {
        if (request.readyState === 4) {
            clearTimeout(xhrTimeout);
            if (request.status >= 200 && request.status < 300) {
                if (isFunction(onSuccess)) {
                    onSuccess(request);
                }
            } else {
                if (isFunction(onFailure)) {
                    onFailure(request);
                }
            }
        }
    };
    request.open('GET', url + (url.indexOf('?') >= 0 ? '&' : '?') + paramString, true);
    if (headers) {
        for (name in headers)
            this.transport.setRequestHeader(name, headers[name]);
    }
    request.send(null);
    xhrTimeout = setTimeout(onTimeout, 5000);
}

//function xhr_get(url, params, onSuccess, onFailure, headers) {
//    $.ajax({
//        url: url,
//        type: 'GET',
//        data: params,
//        dataType: 'text',
//        success: function(data, textStatus, jqXHR) {
//            onSuccess(jqXHR);
//        },
//        error: function(jqXHR) {
//            onFailure(jqXHR);
//        },
//        headers: headers
//    });
//}

// Takes an ISO time and returns a string representing how
// long ago the date represents.
function prettyDate(time) {
    var date = new Date((time || "").replace(/-/g,"/").replace(/[TZ]/g," ")),
        diff = (((new Date()).getTime() - date.getTime()) / 1000),
        day_diff = Math.floor(diff / 86400);

    if ( isNaN(day_diff) || day_diff < 0 || day_diff >= 31 )
        return '';

    return day_diff == 0 && (
            diff < 60 && "just now" ||
            diff < 120 && "1 minute ago" ||
            diff < 3600 && Math.floor( diff / 60 ) + " minutes ago" ||
            diff < 7200 && "1 hour ago" ||
            diff < 86400 && Math.floor( diff / 3600 ) + " hours ago") ||
        day_diff == 1 && "Yesterday" ||
        day_diff < 7 && day_diff + " days ago" ||
        day_diff < 31 && Math.ceil( day_diff / 7 ) + " weeks ago";
}