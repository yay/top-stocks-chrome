/**
 *  Top Stocks
 *  Google Chrome Store ID: kpcgmhpmdinmidgkkiekbochocibaejn
 *  
 */

isDevice = 'blackberry' in window; // requires <feature id="blackberry" /> in config.xml

// TopStocks namespace
TopStocks = {};

// Constants
//TopStocks.versionString = '1.0.0';
TopStocks.isTrial = false;
TopStocks.trialStartTime = 0;
TopStocks.settingsItem = 'TopStocksSettings';
TopStocks.listSaveInterval = 280000;
//TopStocks.alertSoundDefaultFile = "/media/internal/ringtones/Flurry.mp3";
//TopStocks.alertSoundDefaultTitle = "Flurry";
TopStocks.dummy = "- - - - - - - - -";
TopStocks.tickerStageName = 'topStocksTicker';
TopStocks.multiLine = 'multi-line';
TopStocks.visible = 'visible';
TopStocks.truncatingText = 'truncating-text';
TopStocks.errorNone = '0';                     // No error, success
TopStocks.invalidFeedError = '1';              // Not RSS2, RDF (RSS1), or ATOM
TopStocks.stockChartUrl = "http://www.google.com/finance/chart?cht=o&q=#{exchange}%3A#{symbol}&tlf=#{timeLineFormat}&chs=m&p=#{period}&client=sfa#{append}";
TopStocks.indexChartUrl = "http://www.google.com/finance/chart?cht=c&chs=#{chartSize}&q=INDEXDJX%3A.DJI%2CINDEXSP%3A.INX%2CINDEXNASDAQ%3A.IXIC&tlf=#{timeLineFormat}&client=sfa#{append}";
TopStocks.yahooNewsUrl = "http://finance.yahoo.com/rss/";
TopStocks.newsProviders = [
    {
        name: 'Reuters News',
        baseUrl: 'http://feeds.reuters.com/',
        feeds: [
            {title: "Markets", itemClass: 'static listCut'},
            {title: "Economy", value: "news/economy"},
            {title: "Global Markets", value: "reuters/globalmarketsNews"},
            {title: "Regulatory News", value: "reuters/governmentfilingsNews"},
            {title: "US Markets", value: "news/usmarkets"},
            {title: "US Dollar Report", value: "reuters/USdollarreportNews"},
//            {title: "Hot Stocks", value: "reuters/hotStocksNews"},
            {title: "Deals", value: "news/deals"},
            {title: "Mergers & Acquisitions", value: "reuters/mergersNews"},
//            {title: "IPOs", value: "reuters/newIssuesNews"},
            {title: "Bonds", value: "reuters/bondsNews"},
            {title: "Hedge Funds", value: "news/hedgefunds"},
            {title: "Private Equity", value: "reuters/privateequityNews"},
            {title: "Small Business", value: "reuters/smallBusinessNews"},
            {title: "Summit News", value: "reuters/summitNews"},
            {title: "Financial Services & Real Estate", value: "reuters/financialServicesrealEstateNews"},
            {title: "Bankruptcy", value: "reuters/bankruptcyNews"},
            
            {title: "Sectors", itemClass: 'static listCut'},
            {title: "Basic Materials", value: "reuters/basicmaterialsNews"},
            {title: "Cyclical Consumer Goods", value: "reuters/cyclicalconsumergoodsNews"},
            {title: "Non-Cyclical Consumer Goods", value: "reuters/noncyclicalconsumergoodsNews"},
            {title: "Energy", value: "reuters/USenergyNews"},
            {title: "Environment", value: "reuters/environment"},
            {title: "Financials", value: "reuters/financialsNews"},
            {title: "Healthcare", value: "reuters/UShealthcareNews"},
            {title: "Industrials", value: "reuters/industrialsNews"},
            {title: "Media Diversified", value: "reuters/USmediaDiversifiedNews"},
            {title: "Technology", value: "reuters/technologysectorNews"},
            {title: "Telecommunications", value: "reuters/telecomsectorNews"},
            {title: "Utilities", value: "reuters/utilitiesNews"}
        ]
    },
    {
        name: 'Yahoo! Finance',
        baseUrl: 'http://finance.yahoo.com/rss/',
        feeds: [
            {title: "U.S. Markets", value: "usmarkets"},
            {title: "Most Popular", value: "mostpopular"},
            {title: "Earnings", value: "earnings"},
            {title: "Upgrades & Downgrades", value: "upgradesdowngrades"},
            {title: "IPOs", value: "IPO"},
            {title: "Mergers & Acquisitions", value: "mergersaquisitions"},
            {title: "Economy & Government", value: "economy"},
            {title: "International Finance", value: "international"},
            {title: "Bonds", value: "bonds"},
            {title: "Commodities", value: "commodities"},
            {title: "Currencies", value: "currencies"},
            {title: "Funds", value: "funds"},
            {title: "Venture Capital", value: "venturecapital"},
            {title: "Taxes", value: "taxes"},
            {title: "Options", value: "options"},
            {title: "Investing Picks", value: "investingpicks"},
            {title: "Investing Strategies", value: "investingstrategies"},
            {title: "Investing Ideas", value: "investingideas"},
            {title: "Banking", value: "banking"},
            {title: "Mortgages", value: "mortgages"},
            {title: "Retirement", value: "retirement"}
        ]
    }
];

TopStocks.cacheCharts = false;
TopStocks.showMiniCharts = true;
TopStocks.showRecentTrend = true;
TopStocks.showExtHours = true;

TopStocks.realTimeExchanges = ['NYSE', 'NASDAQ', 'NYSEAMEX', 'NYSEARCA', 'CNSX',
    'LON', 'BIT', 'BOM', 'NSE', 'SHA', 'SHE', 'TPE'];
TopStocks.realTimeIndexes = ['INDEXDJX', 'INDEXSP', 'INDEXNASDAQ', 'SHA',
    'INDEXBOM', 'INDEXASX'];

//TopStocks.zoomLevels = ['1d', '5d', '1M', '6M', '1Y', '5Y', 'max'];
TopStocks.updateIntervals = [0, 10000, 30000, 60000, 300000, 900000, 1800000];

// Persistent Globals - will be saved across app launches
TopStocks.updateInterval = 10000;
TopStocks.chartZoom = '1d';
TopStocks.sortMethod = 0;
TopStocks.sortOrder = 0;
TopStocks.lastStock = 'NASDAQ:RIMM';
TopStocks.lastIndex = 'INDEXDJX:.DJI';
TopStocks.newsProviderIndex = 0;
TopStocks.lastFeedIndex = 1;
TopStocks.lastScene = 'watchlist';

//TopStocks.showSearchTip = true;
//TopStocks.idCollected = false;
//TopStocks.showTicker = false;
//TopStocks.tickerIndex = 0;
//TopStocks.tickerList = 0;
//TopStocks.tickerStyle = 0;
//TopStocks.tickerRotation = 5000;

//TopStocks.googleUsername = '';
//TopStocks.googlePassword = '';

//  Session Globals - not saved across app launches
TopStocks.timeZone = '';
TopStocks.lastMiniChartTime = 0;
TopStocks.updateTaskId = 0;                         // ID of the list update timeout
TopStocks.firstLaunch = true;                       // First launch of this version of the app
//TopStocks.isInternetConnectionAvailable = false;


// Helper functions
TopStocks.getChangeColor = function(last, previousLast) {
    if (last && previousLast) {
        last = Number(last.replace(/,/g, ''));
        previousLast = Number(previousLast.replace(/,/g, ''));
    }
    if (last && previousLast) {
        var value = last - previousLast;
        if (value > 0) {
            return 'green'
        } else if (value < 0) {
            return 'red'
        }
    }
    return '';
};

// mini charts are cached, but we want them to be updated frequently,
// so we alter the URL from time to time by providing a unique extra parameter
TopStocks.getMiniChartTime = function() {
    var now = (new Date()).getTime();
    // mini chart is 50px wide and market session is 6:30 (390 minutes),
    // so 390 / 50 = 7.8 minutes per chart's pixel
    // but we want to upate twice as often than that, since that last pixel
    // can fluctuate a lot for volitile stocks in that 7.8 minute period
    if (now - TopStocks.lastMiniChartTime > Math.floor(3.9 * 60 * 1000)) {
        TopStocks.lastMiniChartTime = now;
    }
    return TopStocks.lastMiniChartTime;
};

TopStocks.scheduleQuotesUpdate = function() {
    if (TopStocks.updateInterval > 0) {
        var interval = TopStocks.updateInterval;
        TopStocks.updateTaskId = setTimeout(TopStocks.updateQuotes, interval);
//        console.info('Quotes update scheduled in', interval/1000, 'seconds.');
    } else {
        console.info('Automatic quote updates have been stopped.');
    }
};

TopStocks.updateQuotes = function() {
    TopStocks.scheduleQuotesUpdate();
    
    Quotes.updateQuotes(Quotes.watchList);
    Quotes.updateQuotes(Quotes.indexList);
};

TopStocks.nativeAlert = function(text, title, callback) {
    if (isDevice) {
        try {
            blackberry.ui.dialog.standardAskAsync(text,
                blackberry.ui.dialog.D_OK, callback,
                {
                    title : title,
                    size: blackberry.ui.dialog.SIZE_SMALL,
                    position : blackberry.ui.dialog.LOC_BOTTOM
                }
            );
        } catch (e) {
            alert('Exception in blackberry.ui.dialog: ' + e);
        }
    } else {
        alert(text);
    }
};

TopStocks.nativeOpenUrl = function(url) {
    if (!(typeof url === 'string')) return;
    if (isDevice) {
        var args = new blackberry.invoke.BrowserArguments(url);
        blackberry.invoke.invoke(blackberry.invoke.APP_BROWSER, args);
    } else {
        window.open(url);
    }
};

TopStocks.main = function() {
    var now = (new Date()).getTime(),
        msDay = 24 * 60 * 60 * 1000, // milliseconds in a day
        daysLeft = 3,
        doExit = false;
    
    function changeMainTab(sceneName, clickedTab) {
        if (Director.canChangeScene(sceneName)) {
//            $('#settings_tab').removeClass('selected');
            clickedTab.parent().find('li').removeClass('selected')
            clickedTab.addClass('selected');
            
            Director.changeScene(sceneName);
            return true;
        } else {
            return false;
        }
    }
//    $('#settings_tab').on('click', function() {
//        if (Director.canChangeScene('settings')) {
//            $('#main_tabs li').removeClass('selected');
//            $(this).addClass('selected');
//            Director.changeScene('settings');
//        }
//    });
    $('#main_tabs li').on('click', function() {
        switch ($(this).html()) {
            case 'Markets':
                changeMainTab('markets', $(this));
                break;
            case 'Watchlist':
                changeMainTab('watchlist', $(this));
                break;
            case 'News':
                changeMainTab('news', $(this));
                break;
            default:
                break;
        }
    });
    
    TopStocks.loadSettings();
    Director.changeScene('watchlist');
    TopStocks.updateQuotes();
    
    if (isDevice) {
        if (TopStocks.isTrial) {
            $('#trial').show();
            if (TopStocks.firstLaunch) {
                TopStocks.trialStartTime = (new Date()).getTime();
                TopStocks.firstLaunch = false;
            } else {
                if (now - TopStocks.trialStartTime >= 3 * msDay) {
                    doExit = true;
                    $('#trial_days').html('Your trial has ended.');
                }
            }
        } else {
            $('#welcome').show();
        }
        
        if (!doExit) {
            if (TopStocks.isTrial) {
                daysLeft = Math.ceil((3 * msDay - (now - TopStocks.trialStartTime)) / (24 * 60 * 60 * 1000));
                if (daysLeft > 3) daysLeft = 3;
                $('#trial_days').html('Your trial will expire in ' +
                    daysLeft.toString(10) + (daysLeft > 1 ? ' days.' : ' day.'));
            }
            setTimeout(function() {
                $('#touch_blocker').fadeOut(1000, function() {
                    $(this).removeClass('white');
                });
            }, TopStocks.isTrial ? 5000 : 1000);
        }
    } else {
        TopStocks.initMobileAppButton();
//        $('#touch_blocker').hide();
        setTimeout(function() {
//            $('#touch_blocker').fadeOut(1000);
            $('#touch_blocker').animate({top: '-200%', opacity: 0}, 800, function() {
                $(this).hide();
            });
        }, 1000);
    }
};

TopStocks.initMobileAppButton = function() {
    var mobileApp = $('#mobileApp'),
        mobileAppTip = $('#mobileAppTip'),
        showTip = function() {
            if (TopStocks.hideMobileAppTipTask) {
                clearTimeout(TopStocks.hideMobileAppTipTask);
                delete TopStocks.hideMobileAppTipTask;
                return;
            }
            mobileAppTip.css('opacity', 0)
                        .css('margin-top', -20)
                        .css('left', $(this).position().left - 128)
                        .css('top', $(this).position().top + $(this).height() + 14)
                        .show()
                        .animate({
                            'opacity': 1,
                            'margin-top': 0
                        }, 200);
        },
        hideTip = function() {
            if (TopStocks.hideMobileAppTipTask || !mobileAppTip.is(':visible')) {
                return; // aready hidden or scheduled to hide
            }
            TopStocks.hideMobileAppTipTask = setTimeout(function() {
                delete TopStocks.hideMobileAppTipTask;
                mobileAppTip.animate({opacity: 0}, 200, function() {
                    $(this).hide();
                });
            }, 1000);
        };
    mobileApp.on('click', showTip);
    mobileApp.on('mouseleave', hideTip);
    mobileAppTip.on('mouseenter', showTip);
    mobileAppTip.on('mouseleave', hideTip);
};

TopStocks.loadSettings = function() {
    console.log('Loading settings...');
    var oldPrefs = localStorage.getItem(TopStocks.settingsItem);
    if (oldPrefs) {
        try {
            oldPrefs = JSON.parse(oldPrefs);
        } catch (e) {
            console.error('JSON.parse error:', e.message);
            return; // use default settings
        }
    } else {
        return; // use default settings
    }
    
    // load whatever settings are found, use defaults for the rest
    function loadProperty(prop) {
        if (prop in oldPrefs) TopStocks[prop] = oldPrefs[prop];
    }
    
    loadProperty('updateInterval');
    loadProperty('chartZoom');
    loadProperty('sortMethod');
    loadProperty('sortOrder');
    loadProperty('lastStock');
    loadProperty('lastIndex');
    loadProperty('newsProviderIndex');
    loadProperty('lastFeedIndex');
    loadProperty('firstLaunch');
    loadProperty('trialStartTime');
};

TopStocks.saveSettings = function() {
    console.log('Saving settings...');
    localStorage.setItem(TopStocks.settingsItem, JSON.stringify({
        firstLaunch: TopStocks.firstLaunch,
        trialStartTime: TopStocks.trialStartTime,
        updateInterval: TopStocks.updateInterval,
        chartZoom: TopStocks.chartZoom,
        sortMethod: TopStocks.sortMethod,
        sortOrder: TopStocks.sortOrder,
        lastStock: TopStocks.lastStock,
        lastIndex: TopStocks.lastIndex,
        newsProviderIndex: TopStocks.newsProviderIndex,
        lastFeedIndex: TopStocks.lastFeedIndex
//        lastScene: TopStocks.lastScene,
        
    }));
};

TopStocks.onBackgroundCallback = function () {
    $('body').css('background-color', '#e9e9e9');
};

TopStocks.onForegroundCallback = function () {
    $('body').css('background-color', '#8c8c8c');
};

window.addEventListener('load', TopStocks.main);
if (isDevice) {
    blackberry.app.event.onBackground(TopStocks.onBackgroundCallback);
    blackberry.app.event.onForeground(TopStocks.onForegroundCallback);
} else {
//    // confirm tab close
//    window.addEventListener('beforeunload', function() {
//        TopStocks.saveSettings();
////        localStorage.clear();
//        return '';
//    });
}