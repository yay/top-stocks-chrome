/*

  list[x].feed        Object

  feed.title          String    The title of a feed. Don't really need it.
  feed.url            String    Feed source URL in unescaped form
  feed.type           String    Feed type: either rdf (rss1), rss (rss2) or atom
  feed.value          Boolean   Spinner model for feed update indicator
  feed.stories        Array     Each entry is a complete story

  stories[y].title    String    Story title or headline
  stories[y].text     String    Story text
  stories[y].summary  String    Story text, stripped of markup
  stories[y].url      String    Story url

*/

var Quotes = {
    authUrl: "https://www.google.com/accounts/ClientLogin",
    quoteUrl: "http://www.google.com/finance/info",
    companyFeedUrl: "http://www.google.com/finance/company_news?q=#{exchange}:#{symbol}&output=rss",
    lookupUrl: "http://www.google.com/finance/match",
//    pricesUrl: "http://www.google.com/finance/getprices?e=#{exchange}&q=#{symbol}&p=#{period}&i=#{interval}",
    chartDataUrl: "http://www.google.com/finance/getprices?e=#{exchange}&q=#{symbol}&p=#{period}&i=#{interval}",
    portfolios: [],
    newsFeed: {stories: []},
    watchList: [],
    defaultList: [
        {symbol: "AAPL", exchange: "NASDAQ"},
        {symbol: "GOOG", exchange: "NASDAQ"},
        {symbol: "RIMM", exchange: "NASDAQ"},
        {symbol: "MSFT", exchange: "NASDAQ"},
        {symbol: "NOK", exchange: "NYSE"},
        {symbol: "AMZN", exchange: "NASDAQ"}
    ],
    indexList: [
        {symbol: ".DJI", exchange: "INDEXDJX"},
        {symbol: ".IXIC", exchange: "INDEXNASDAQ"},
        {symbol: ".INX", exchange: "INDEXSP"},
        {symbol: "NYA", exchange: "INDEXDJX"},

        {symbol: "OSPTX", exchange: "TSE"},       // Toronto
        {symbol: "UKX", exchange: "INDEXFTSE"},   // UK, FTSE 100
        {symbol: "PX1", exchange: "INDEXEURO"},   // France, CAC 40
        {symbol: "SX5E", exchange: "INDEXSTOXX"}, // Stoxx Europe 50 (ETF benchmark)
        {symbol: "XJO", exchange: "INDEXASX"},    // Australia

        {symbol: "000001", exchange: "SHA"},         // Shanghai Stock Exchange
        {symbol: "NI225", exchange: "INDEXNIKKEI"},  // Tokyo, Nikkei 225
        {symbol: "SENSEX", exchange: "INDEXBOM"},    // Bombay, SENSEX
        {symbol: "TAIEX", exchange: "TPE"},          // Taiwan
        {symbol: "HSI", exchange: "INDEXHANGSENG"}   // Hong Kong, Hang Seng
    ],
    previewList: [],
    assistants: {},
    watchListLastSaveTime: 0,
    indexListLastSaveTime: 0,
    watchListLastUpdateTime: 0,
    initialize: function() {
//        this.registerAjaxResponders();
        this.dbLoad();
    },
    registerAjaxResponders: function() {
        function callInProgress(transport) {
            switch (transport.readyState) {
                case 1: case 2: case 3:
                    return true;
                    break;
                // case 0 and 4
                default:
                    return false;
                    break;
            }
        }

        Ajax.Responders.register({
            onCreate: function(request) {
                request["timeoutId"] = window.setTimeout(
                    function() {
                        // if we have hit the timeout and the AJAX request is active, abort it
                        if (callInProgress(request.transport)) {
                            request.transport.abort();
                            // run the onFailure method if we set one up when creating the AJAX object
                            if (request.options["onFailure"]) {
                                request.options["onFailure"](request.transport);
                            }
                        }
                    },
                    15000
                );
            },
            onComplete: function(request) {
                // clear the timeout, the request completed OK
                window.clearTimeout(request["timeoutId"]);
            }
        });
    },
    getDefaultList: function() {
        var i, returnList = [];
        for (i = 0; i < this.defaultList.length; i++) {
            returnList[i] = this.defaultList[i];
            returnList[i].feed = {};
            returnList[i].feed.stories = [];
            returnList[i].alerts = [];
            returnList[i].feed.url = this.companyFeedUrl.interpolate(returnList[i]);
        }
        return returnList;
    },
    getListName: function(list) {
        switch (list) {
            case this.watchList:
                return "watch list";
            case this.indexList:
                return "index list";
        }
    },
    // updateQuotes(list, startIndex, endIndex) - updates quote with specified index
    //     if two parameters were given, updates range of quotes if three parameters
    //     were given, otherwise updates the whole list
    updateQuotes: function(list, startIndex, endIndex) {
        var i, symbols = [];
        if (list.length == 0) {return}
        if (arguments.length === 2) {
            endIndex = startIndex;
        } else if (arguments.length === 3) {
            if (endIndex - startIndex > 99) {
                endIndex = startIndex + 99;
            }
        } else {
            startIndex = 0;
            endIndex = Math.min(99, list.length - 1);
        }
        for (i = startIndex; i <= endIndex; i++) {
            symbols.push(list[i].exchange + ":" + list[i].symbol);
        }

//        new Ajax.Request(this.quoteUrl, {
//            method: "get",
//            parameters: {
//                client: "ig",
//                infotype: "infoquoteall",
//                q: symbols.join()
//            },
//            evalJSON: false,
//            onSuccess: this.updateQuotesSuccess.bind(this, list),
//            onFailure: this.updateQuotesFailure.bind(this, list)
//        });
        
        xhr_get(this.quoteUrl,
            {
                client: "ig",
                infotype: "infoquoteall",
                q: symbols.join()
            },
            this.updateQuotesSuccess.bind(this, list),
            this.updateQuotesFailure.bind(this, list)
        );
    },
    updateQuotesSuccess: function(list, transport) {
//        console.info("Update %s request success.", this.getListName(list));
        this.processQuotes(list, transport);
        this.updateViews(list, true);
        this.saveList(list);
    },
    updateQuotesFailure: function(list, transport) {
        console.warn("Update %s request failed. responseText: %s",
            this.getListName(list), transport.responseText);
        this.updateViews(list, false);
    },
    updateQuotesEx: function(list, handlers, startIndex, endIndex) {
        var i, symbols = [];
        if (list.length == 0) {return}
        if (arguments.length === 3) {
            endIndex = startIndex;
        } else if (arguments.length === 4) {
            if (endIndex - startIndex > 99) {
                endIndex = startIndex + 99;
            }
        } else {
            startIndex = 0;
            endIndex = Math.min(99, list.length - 1);
        }
        for (i = startIndex; i <= endIndex; i++) {
            symbols.push(list[i].exchange + ":" + list[i].symbol);
        }

//        new Ajax.Request(this.quoteUrl, {
//            method: "get",
//            parameters: {
//                client: "ig",
//                infotype: "infoquoteall",
//                q: symbols.join()
//            },
//            evalJSON: false,
//            onSuccess: this.updateQuotesExSuccess.bind(this, list, handlers.onSuccess),
//            onFailure: this.updateQuotesExFailure.bind(this, list, handlers.onFailure)
//        });
        
        xhr_get(this.quoteUrl,
            {
                client: "ig",
                infotype: "infoquoteall",
                q: symbols.join()
            },
            this.updateQuotesExSuccess.bind(this, list, handlers.onSuccess),
            this.updateQuotesExFailure.bind(this, list, handlers.onFailure)
        );
    },
    updateQuotesExSuccess: function(list, onSuccess, transport) {
        console.info("updateQuotesExSuccess");
        this.processQuotes(list, transport);
        if (isFunction(onSuccess)) {
            onSuccess();
        }
    },
    updateQuotesExFailure: function(list, onFailure, transport) {
        console.info("updateQuotesExFailure", transport.responseText);
        if (isFunction(onFailure)) {
            onFailure();
        }
    },
    // saveList - checks if list should be saved, saves list if true
    saveList: function(list, noCheck) {
        // save lists if never saved, or manual updates or... time to save
        var currentTime = (new Date()).getTime();
        if (list === this.watchList) {
            if (noCheck === true ||
                this.watchListLastSaveTime == 0 ||
                TopStocks.updateInterval == 0 ||
                currentTime - this.watchListLastSaveTime > TopStocks.listSaveInterval) {
                this.watchListLastSaveTime = currentTime;
                this.dbStoreWatchList();
            }
        } else if (list === this.indexList) {
            if (noCheck === true ||
                this.indexListLastSaveTime == 0 ||
                TopStocks.updateInterval == 0 ||
                currentTime - this.indexListLastSaveTime > TopStocks.listSaveInterval) {
                this.indexListLastSaveTime = currentTime;
                this.dbStoreIndexList();
            }
        }
    },
    updateNewsFeed: function(url) {
        console.info("updateNewsFeed request initializing...");
        this.newsFeed.stories = [];
        xhr_get(url, null,
            this.updateNewsFeedSuccess.bind(this),
            this.updateNewsFeedFailure.bind(this)
        );
    },
    updateNewsFeedSuccess: function(transport) {
        var feedError = this.processFeed(transport, this.newsFeed);
        if (feedError !== TopStocks.errorNone) {
            if (feedError == TopStocks.invalidFeedError)
                console.warn("Feed type is not a supported.");
            if (this.assistants.news) this.assistants.news.updateModelCallback();
        } else {
            this.composeNewsStorySummary(this.newsFeed.stories);
            if (this.assistants.news) this.assistants.news.updateModelCallback();
        }
    },
    updateNewsFeedFailure: function(transport) {
        console.info("Update news request failed.");
        // probably no internet access, still want to update the view to show the saved feed
        if (this.assistants.news !== undefined) {
            this.assistants.news.updateModelCallback();
        }
    },
    updateCompanyFeed: function(list, index) {
        xhr_get(list[index].feed.url, null,
            this.updateCompanyFeedSuccess.bind(this, list, index),
            this.updateCompanyFeedFailure.bind(this, list, index)
        );
    },
    updateCompanyFeedSuccess: function(list, index, transport) {
        // process the feed, passing in transport holding the updated feed data
        var feedError = this.processFeed(transport, list[index].feed);

        // if successful processFeed returns TopStocks.errorNone
        if (feedError !== TopStocks.errorNone) {
            if (feedError == TopStocks.invalidFeedError) {
                console.info("Feed", list[index].feed.url,
                    "is not a supported feed type");
            }
        } else {
            this.composeCompanyStorySummary(list[index].feed.stories);
            if (this.assistants.quoteList !== undefined) {
                this.assistants.quoteList.updateStoryModel(index);
            } else if (this.assistants.markets !== undefined) {
                this.assistants.markets.updateStoryModel(index);
            }
        }
    },
    updateCompanyFeedFailure: function(list, index, transport) {
        console.info("Update company feed request failed. responseText:",
            transport.responseText);
        // probably no internet access, still want to update the view to show the saved feed
        if (this.assistants.quoteList !== undefined) {
            this.assistants.quoteList.updateStoryModel(index);
        } else if (this.assistants.markets !== undefined) {
            this.assistants.markets.updateStoryModel(index);
        }
    },
    // Supports RSS, RDF and Atom feed formats.
    processFeed: function(transport, feed) {
        var doc;
        // work around due to occasional XML errors
        if (transport.responseXML === null && transport.responseText !== null) {
            console.info("Request not in XML format - manually converting");
            doc = new DOMParser().parseFromString(transport.responseText,
                transport.responseText.substr(0, 2) === '<?' ? "text/xml" : "text/html");
            if (!doc) return TopStocks.invalidFeedError;
        } else {
            doc = transport.responseXML;
        }
        // Used to hold feed list as it's processed from the Ajax request
        var listItems = [];
        // Variable to hold feed type tags
        var feedType = doc.getElementsByTagName("rss");

        // Determine whether RSS 2, RDF (RSS 1) or ATOM feed
        if (feedType.length > 0) {
            feed.type = "rss";
        } else {
            feedType = doc.getElementsByTagName("RDF");
            if (feedType.length > 0) {
                feed.type = "RDF";
            } else {
                feedType = doc.getElementsByTagName("feed");
                if (feedType.length > 0) {
                    feed.type = "atom";
                } else {
                    // If none of those then it can't be processed, set an error code
                    // in the result, log the error and return
                    console.warn("Unsupported feed format.");
                    return TopStocks.invalidFeedError;
                }
            }
        }

        // Process feeds; retain title, text content and url
        var i;
        switch (feed.type) {
            case "atom":
                // Temp object to hold incoming XML object
                var atomEntries = doc.getElementsByTagName("entry");
                for (i = 0; i < atomEntries.length; i++) {
                    listItems[i] = {
                        title: unescape(atomEntries[i].getElementsByTagName("title").item(0).textContent),
                        text: atomEntries[i].getElementsByTagName("content").item(0).textContent,
                        url: atomEntries[i].getElementsByTagName("link").item(0).getAttribute("href")
                    };

                    // Strip HTML from text for summary and shorten to 100 characters
                    listItems[i].summary = listItems[i].text.replace(/(<([^>]+)>)/ig,"");
                    listItems[i].summary = listItems[i].summary.replace(/http:\S+/ig,"");
                    listItems[i].summary = listItems[i].summary.replace(/#[a-z]+/ig,"{");
                    listItems[i].summary = listItems[i].summary.replace(/(\{([^\}]+)\})/ig,"");
                    listItems[i].summary = listItems[i].summary.replace(/digg_url .../,"");
                    listItems[i].summary = unescape(listItems[i].summary);
                    listItems[i].summary = listItems[i].summary.substring(0, 101);
                }
                break;

            case "rss":
                // Temp object to hold incoming XML object
                var rssItems = doc.getElementsByTagName("item");
                for (i = 0; i < rssItems.length; i++) {

                    listItems[i] = {
                        title: unescape(rssItems[i].getElementsByTagName("title").item(0).textContent),
                        text: rssItems[i].getElementsByTagName("description").item(0).textContent,
                        url: rssItems[i].getElementsByTagName("link").item(0).textContent
                    };
                    var pubDateTags = rssItems[i].getElementsByTagName("pubDate");
                    if (pubDateTags.length > 0) {
                        listItems[i].date = prettyDate(pubDateTags.item(0).textContent);
                    }
//                    alert(JSON.stringify(listItems[i]));
                    // Strip HTML from text for summary and shorten to 100 characters
                    // remove images
                    listItems[i].text = listItems[i].text.replace(/<img\b[^>]*>/ig, '');
                    // remove links
                    listItems[i].text = listItems[i].text.replace(/<a\b[^>]*>(.*?)<\/a>/ig,
                        function() {
                            return arguments[1] ? '<span class="bold">' + arguments[1] + '</span>' : '';
                        });
                    // replace paragraphs with divs
                    listItems[i].text = listItems[i].text.replace(/<p\b[^>]*>(.*?)<\/p>/ig,
                        function() {
                            return arguments[1] ? '<div>' + arguments[1] + '</div>' : '';
                        });
                    // TODO: remove <b></b> tags
//                    listItems[i].text = listItems[i].text.replace(/&lt;b$gt;\b[^>]*>(.*?)&lt\/b$gt;/ig,
//                        function() { return arguments[1]; });
//                    alert(JSON.stringify(listItems[i]));
//                    listItems[i].summary = listItems[i].text.replace(/(<([^>]+)>)/ig,"");
//                    listItems[i].summary = listItems[i].summary.replace(/http:\S+/ig,"");
//                    listItems[i].summary = listItems[i].summary.replace(/#[a-z]+/ig,"{");
//                    listItems[i].summary = listItems[i].summary.replace(/(\{([^\}]+)\})/ig,"");
//                    listItems[i].summary = listItems[i].summary.replace(/digg_url .../,"");
//                    listItems[i].summary = unescape(listItems[i].summary);
//                    listItems[i].summary = listItems[i].summary.substring(0, 101);
                }
                break;

            case "RDF":
                // Temp object to hold incoming XML object
                var rdfItems = doc.getElementsByTagName("item");
                for (i = 0; i < rdfItems.length; i++) {

                    listItems[i] = {
                        title: unescape(rdfItems[i].getElementsByTagName("title").item(0).textContent),
                        text: rdfItems[i].getElementsByTagName("description").item(0).textContent,
                        url: rdfItems[i].getElementsByTagName("link").item(0).textContent
                    };

                    // Strip HTML from text for summary and shorten to 100 characters
                    listItems[i].summary = listItems[i].text.replace(/(<([^>]+)>)/ig,"");
                    listItems[i].summary = listItems[i].summary.replace(/http:\S+/ig,"");
                    listItems[i].summary = listItems[i].summary.replace(/#[a-z]+/ig,"{");
                    listItems[i].summary = listItems[i].summary.replace(/(\{([^\}]+)\})/ig,"");
                    listItems[i].summary = listItems[i].summary.replace(/digg_url .../,"");
                    listItems[i].summary = unescape(listItems[i].summary);
                    listItems[i].summary = listItems[i].summary.substring(0, 101);
                }
                break;
        }

        // save updated feed
        feed.stories = listItems;

        // If new feed, the user may not have entered a name; if so, set the name to the feed title
        if (feed.title === "")    {
            // Will return multiple hits, but the first is the feed name
            var titleNodes = doc.getElementsByTagName("title");
            feed.title = titleNodes[0].textContent;
        }
        return TopStocks.errorNone;
    },
    composeCompanyStorySummary: function(stories) {
        for (var i = 0; i < stories.length; i++) {
            // Google Finance Company News Feed specific code
            // replace first link with its text
            stories[i].summary = stories[i].text.replace(/([^<]*)<a[^<]*>([^<]*)<\/a>([^<]*)/, function() {
                return arguments[1] + '<span class="story-summary">' +
                    arguments[2] + '</span>' + arguments[3];
            });
            // remove unwanted elements after the inner div
            stories[i].summary = stories[i].summary.replace(/<\/div\s*>(\S|\s)*<\/div\s*>/, "</div></div>");
            stories[i].summary = stories[i].summary.replace(/color:#888888/g, "color: gray");
            stories[i].summary = keepOnlyStyle(stories[i].summary, ["color"]);
            // no longer need the text in this case
            stories[i].text = "";
        }
    },
    composeNewsStorySummary: function(stories) {
        for (var i = 0; i < stories.length; i++) {
//            stories[i].summary = stories[i].text;
            stories[i].summary = stories[i].text.replace(/<a(\s[^>]*)?>.*?<\/a>/ig, "");
            stories[i].summary = stories[i].summary.replace(/<p[^<]*>([^<]*)<\/p>/, function() {
                return arguments[1];
            });
            stories[i].text = "";
        }
    },
    // addQuotes(symbols) - adds quotes with given symbols to the list
    addQuotes: function(symbols, onSuccess, onFailure) {
//        new Ajax.Request(this.quoteUrl, {
//            method: "get",
//            parameters: {
//                client: "ig",
//                infotype: "infoquoteall",
//                q: symbols
//            },
//            evalJSON: false,
//            "onSuccess": this.addQuotesSuccess.bind(this, onSuccess),
//            "onFailure": this.addQuotesFailure.bind(this, onFailure)
//        });
        xhr_get(this.quoteUrl,
            {
                client: "ig",
                infotype: "infoquoteall",
                q: symbols
            },
            this.addQuotesSuccess.bind(this, onSuccess),
            this.addQuotesFailure.bind(this, onFailure)
        );
    },
    addQuotesSuccess: function(onSuccess, transport) {
        this.processQuotes(this.watchList, transport, true);
        this.updateListModel(this.watchList, true);
        this.dbStoreWatchList();
        if (isFunction(onSuccess)) {
            onSuccess();
        }
    },
    addQuotesFailure: function(onFailure, transport) {
        console.info("Add quotes request failed. responseText:",
            transport.responseText);
        if (this.assistants.quoteList !== undefined) {
            this.assistants.quoteList.showQuoteError();
        }
        if (isFunction(onFailure)) {
            onFailure();
        }
    },
    previewQuotes: function(symbols, onSuccess, onFailure) {
//        new Ajax.Request(this.quoteUrl, {
//            method: "get",
//            parameters: {
//                client: "ig",
//                infotype: "infoquoteall",
//                q: symbols
//            },
//            evalJSON: false,
//            "onSuccess": this.previewQuotesSuccess.bind(this, onSuccess),
//            "onFailure": this.previewQuotesFailure.bind(this, onFailure)
//        });
        xhr_get(this.quoteUrl,
            {
                client: "ig",
                infotype: "infoquoteall",
                q: symbols
            },
            this.previewQuotesSuccess.bind(this, onSuccess),
            this.previewQuotesFailure.bind(this, onFailure)
        );
    },
    previewQuotesSuccess: function(onSuccess, transport) {
        this.processQuotes(this.previewList, transport, true);
        if (isFunction(onSuccess)) {
            onSuccess();
        }
    },
    previewQuotesFailure: function(onFailure, transport) {
        console.info("Preview quotes request failed. responseText:",
            transport.responseText);
        if (isFunction(onFailure)) {
            onFailure();
        }
    },
    getChartData: function(quote, params, onSuccess, onFailure) {
        if (this.isRecentChartData(quote, params.period)) {
            if (isFunction(onSuccess)) onSuccess();
        } else {
//            new Ajax.Request(this.chartDataUrl.interpolate({
//                exchange: quote.exchange,
//                symbol: quote.symbol,
//                period: params.period,
//                interval: params.interval
//            }),
//            {
//                method: "get",
//                evalJSON: false,
//                "onSuccess": this.getChartDataSuccess.bind(this, quote, params, onSuccess),
//                "onFailure": this.getChartDataFailure.bind(this, onFailure)
//            });
            xhr_get(this.chartDataUrl.interpolate({
                    exchange: quote.exchange,
                    symbol: quote.symbol,
                    period: params.period,
                    interval: params.interval
                }), null,
                this.getChartDataSuccess.bind(this, quote, params, onSuccess),
                this.getChartDataFailure.bind(this, onFailure)
            );
        }
    },
    getChartDataSuccess: function(quote, params, onSuccess, transport) {
        var lines = transport.responseText.split("\n"), n = lines.length,
            i, j, pair, columns, price, data;
            
        console.info("Chart data # of lines:", lines.length);

        quote.chartData[params.period] = [];
        data = quote.chartData[params.period][params.interval] = {};
        
        data.dateData = [];
        data.priceData = [];
        data.volumeData = [];
        
        for (i = 0; i < n; i++) {
            if (lines[i].indexOf("=") != -1) {
                pair = lines[i].split("=");
                switch (pair[0]) {
                    case "MARKET_OPEN_MINUTE":
                        data.marketOpenMinute = pair[1];
                        break;
                    case "MARKET_CLOSE_MINUTE":
                        data.marketCloseMinute = pair[1];
                        break;
                    case "COLUMNS":
                        data.columnNames = pair[1].toLowerCase().split(",");
                        break;
                    case "TIMEZONE_OFFSET":
                        data.timezoneOffset = pair[1];
                        break;
                }
            } else {
                columns = lines[i].split(",");
                // isNaN check is there to skip non-number indices
                if (columns.length > 1) {
                    if (isNaN(columns[0])) {

                    } else {
                        price = [];
                        for (j = 1; j < quote.prices.columnNames.length; j++) {
                            price[quote.prices.columnNames[j]] = columns[j];
                        }
                        quote.prices.period["1d"].push(price);
    //                    symbol.prices.period["1d"][parseInt(columns[0])] = price;
                    }
                }
            }
        }
        if (isFunction(onSuccess)) {
            onSuccess(transport);
        }
    },
    getChartDataFailure: function(onFailure, transport) {
        console.info("Getting chart data failed. responseText:", transport.responseText);
        if (isFunction(onFailure)) {
            onFailure(transport);
        }
    },
//    getPrices: function(list, index, params, onSuccess, onFailure) {
//        if (this.isRecentChartData(list[index], params.period)) {
//            if (isFunction(onSuccess)) onSuccess();
//        } else {
//            new Ajax.Request(this.pricesUrl.interpolate({
//                exchange: list[index].exchange,
//                symbol: list[index].symbol,
//                period: params.period,
//                interval: params.interval
//            }),
//            {
//                method: "get",
//                evalJSON: false,
//                "onSuccess": this.getPricesSuccess.bind(this, list, index, onSuccess),
//                "onFailure": this.getPricesFailure.bind(this, list, index, onFailure)
//            });
//        }
//    },
//    getPricesSuccess: function(list, index, onSuccess, transport) {
//        var lines = transport.responseText.split("\n"),
//            symbol = list[index],
//            i, j, pair, columns, price;
//        console.info("LINES LENGTH:", lines.length);
//        symbol.prices.period["1d"] = [];
//        for (i = 0; i < lines.length; i++) {
//            if (lines[i].indexOf("=") != -1) {
//                pair = lines[i].split("=");
//                switch (pair[0]) {
//                    case "MARKET_OPEN_MINUTE":
//                        symbol.prices.marketOpenMinute = pair[1];
//                        break;
//                    case "MARKET_CLOSE_MINUTE":
//                        symbol.prices.marketCloseMinute = pair[1];
//                        break;
//                    case "INTERVAL":
//                        symbol.prices.interval = pair[1];
//                        break;
//                    case "COLUMNS":
//                        symbol.prices.columnNames = pair[1].toLowerCase().split(",");
//                        break;
//                    case "TIMEZONE_OFFSET":
//                        symbol.prices.timezoneOffset = pair[1];
//                        break;
//                }
//            } else {
//                columns = lines[i].split(",");
//                // isNaN check is there to skip non-number indices
//                if (columns.length > 1 && !isNaN(columns[0])) {
//                    price = [];
//                    for (j = 1; j < symbol.prices.columnNames.length; j++) {
//                        price[symbol.prices.columnNames[j]] = columns[j];
//                    }
//                    symbol.prices.period["1d"].push(price);
////                    symbol.prices.period["1d"][parseInt(columns[0])] = price;
//                }
//            }
//        }
//        console.info("FUUU:", symbol.prices.period["1d"][1].close);
//        if (isFunction(onSuccess)) {
//            onSuccess(transport);
//        }
//    },
//    getPricesFailure: function(list, index, onFailure, transport) {
//        console.info("Get prices failed. responseText:", transport.responseText);
//        if (isFunction(onFailure)) {
//            onFailure(transport);
//        }
//    },
    lookupSymbol: function(lookupString, lookupSuccess, lookupFailure) {
//        new Ajax.Request(this.lookupUrl + "?matchtype=matchall&q=" +
//            encodeURIComponent(lookupString), {
//            method: "get",
//            evalJSON: false,
//            onSuccess: lookupSuccess,
//            onFailure: lookupFailure
//        });
        xhr_get(this.lookupUrl + "?matchtype=matchall&q=" +
            encodeURIComponent(lookupString),
            null,
            lookupSuccess,
            lookupFailure
        );
    },
    // processQuotes(transport, add) - adds quotes to the watch list or replaces
    //     current quotes, depending on the value of the "add" flag (boolean)
    processQuotes: function(list, transport, add) {
        var i, n, data, jsonStr = transport.responseText.substr(3), now = new Date();
        try {
            // Google Finance uses invalid characters in their JSON: \xXX instead of \uXXXX
            jsonStr = unescape(jsonStr.replace(/\\x/g, '%'));
            data = JSON.parse(jsonStr);
        } catch (e) {
            console.error('JSON.parse error:', e.message);
            return;
        }
        if (add) {
            for (i = 0; i < data.length; i++) {
                n = list.push({});
                this.mapProperties(list[n - 1], data[i], now);
            }
        } else {
            for (i = 0; i < data.length; i++) {
                this.replaceQuote(list, data[i], now);
            }
        }
    },
    // replaceQuote(quote) - finds quote in the watch list by the symbol of the
    //     given quote and, if found, replaces it with the given quote
    replaceQuote: function(list, quote, now) {
        for (var i = 0, n = list.length; i < n; i++) {
            if (list[i].exchange === quote.e && list[i].symbol === quote.t) {
                this.mapProperties(list[i], quote, now);
                return true;
            }
        }
        return false;
    },
    // indexOfSymbol(symbol) - returns the index of given symbol in the watch/index list
    indexOfSymbol: function(list, symbol) {
        var withExchange = symbol.indexOf(":") !== -1,
            i, n = list.length;
        for (i = 0; i < n; i++) {
            if (withExchange) {
                if (list[i].exchange + ":" + list[i].symbol === symbol) {
                    return i;
                }
            } else {
                if (list[i].symbol === symbol) {
                    return i;
                }
            }
        }
        return -1;
    },
    // checkSymbols(symbols) - validates user input and removes extra spaces,
    //     commas or duplicate quote symbols from the given string
    checkSymbols: function(symbols) {
        // capitalize string (for aesthetics and for Array.unique() to work correctly)
        symbols = symbols.toUpperCase();
        // replace multiple spaces with one space
        symbols = symbols.replace(/\s{2,}/g, " ");
        // replace all spaces with commas
        symbols = symbols.replace(/ /g, ",");
        // remove all leading and trailing commas
        symbols = symbols.replace(/(^,*)|(,*$)/g, "");
        // replace multiple commas with one comma
        symbols = symbols.replace(/,{2,}/g, ",");
        // eliminate duplicate values
        // by comparing with other values in the delimited string
        var arr = symbols.split(",").unique();
        // by comparing with quotes in the watch list
        var i = arr.length - 1;
        while (i >= 0) {
            if (this.indexOfSymbol(this.watchList, arr[i]) >= 0) {
                arr.splice(i, 1);
            }
            i--;
        }
        symbols = arr.join();

        return symbols;
    },
    // mapProperties(x, y) - maps properties from y to x, where y is requested object
    mapProperties: function(x, y, now) {
        x.id                    = y.id;
        x.symbol                = y.t;
        x.exchange              = y.e;
        x.previousLast          = x.last;
        x.last                  = y.l;
        //x.lastWithCurrency      = y.l_cur;
//        console.info(y.t + ": " + y.l_cur);
        x.lastTradeTime         = y.ltt;
        x.lastTradeDateTime     = y.lt;
        x.change                = y.c;
        x.changePercentage      = y.cp;
        x.changeColor           = y.ccol;
        x.changeText            = y.c && !y.c.blank() ?
                                  y.c + (y.cp && !y.cp.blank() ? " (" + y.cp + "%)" : "") :
                                  TopStocks.dummy;

        x.dividend              = y.div;
        x.yield                 = y.yld;
        x.exchangeOpen          = y.eo;
        x.open                  = y.op;
        x.high                  = y.hi;
        x.low                   = y.lo;
        x.volume                = y.vo;
        x.averageVolume         = y.avvo;
        x.high52week            = y.hi52;
        x.low52week             = y.lo52;
        x.marketCap             = y.mc;
        x.PE                    = y.pe;
        x.forwardPE             = y.fwpe;
        x.beta                  = y.beta;
        x.EPS                   = y.eps;
        x.name                  = y.name;
        x.type                  = y.type;

        x.extLast               = y.el;
        //x.extLastWithCurrency   = y.el_cur;
        x.extLastTradeDateTime  = y.elt;
        x.extChange             = y.ec;
        x.extChangePercentage   = y.ecp;
        x.extChangeColor        = y.eccol;
        x.extChangeText         = y.ec && !y.ec.blank() ?
                                  y.ec + (y.ecp && !y.ecp.blank() ? " (" + y.ecp + "%)" : "") :
                                  TopStocks.dummy;

        if (x.feed === undefined) {
            x.feed = {};
            if (x.feed.stories === undefined) {
                x.feed.stories = [];
            }
            if (x.feed.url === undefined) {
                x.feed.url = this.companyFeedUrl.interpolate(x);
            }
        }
        if (x.alerts === undefined) {
            x.alerts = [];
            x.hasAlerts = false;
        } else {
            this.checkAlerts(x, now);
        }
        if (x.prices === undefined) {
            x.prices = {};
            x.prices.period = [];
        }
        if (x.chartData === undefined) {
            x.chartData = [];
        }
    },
    isRecentChartData: function(quote, period) {
        // finding chart data for the specified period
        for (var i = 0; i < quote.chartData.length; i++) {
            switch (quote.chartData[i].period) {
//                case "1d":
//                    return true;
//                    break;
//                case "5d":
//                    return true;
//                    break;
//                case "1M":
//                    return true;
//                    break;
//                case "3M":
//                    return true;
//                    break;
//                case "6M":
//                    return true;
//                    break;
//                case "1Y":
//                    return true;
//                    break;
//                case "5Y":
//                    return true;
//                    break;
                default:
                    return false;
            }
        }
        return false;
    },
    sortWatchList: function() {
        switch (Number(TopStocks.sortMethod)) {
            case 0:
                console.info("Skip sorting");
                break;
            case 1:
                console.info("Sorting by name");
                TopStocks.sortOrder == 0 ?
                    this.watchList.sort(function(a, b) {
                        return a.name.localeCompare(b.name);
                    }) :
                    this.watchList.sort(function(a, b) {
                        return b.name.localeCompare(a.name);
                    });
                break;
            case 2:
                console.info("Sorting by symbol");
                TopStocks.sortOrder == 0 ?
                    this.watchList.sort(function(a, b) {
                        return a.symbol.localeCompare(b.symbol);
                    }) :
                    this.watchList.sort(function(a, b) {
                        return b.symbol.localeCompare(a.symbol);
                    });
                break;
            case 3:
                console.info("Sorting by price");
                TopStocks.sortOrder == 0 ?
                    this.watchList.sort(function(a, b) {
                        return Number(a.last.replace(/,/g, '')) -
                            Number(b.last.replace(/,/g, ''));
                    }) :
                    this.watchList.sort(function(a, b) {
                        return Number(b.last.replace(/,/g, '')) -
                            Number(a.last.replace(/,/g, ''));
                    });
                break;
            case 4:
                console.info("Sorting by change");
                TopStocks.sortOrder == 0 ?
                    this.watchList.sort(function(a, b) {
                        return Number(a.change.replace(/,/g, '')) -
                            Number(b.change.replace(/,/g, ''));
                    }) :
                    this.watchList.sort(function(a, b) {
                        return Number(b.change.replace(/,/g, '')) -
                            Number(a.change.replace(/,/g, ''));
                    });
                break;
            case 5:
                console.info("Sorting by percentage change");
                TopStocks.sortOrder == 0 ?
                    this.watchList.sort(function(a, b) {
                        return Number(a.changePercentage) -
                            Number(b.changePercentage);
                    }) :
                    this.watchList.sort(function(a, b) {
                        return Number(b.changePercentage) -
                            Number(a.changePercentage);
                    });
                break;
        }

    },
    registerAssistant: function(name, assistant) {
        this.assistants[name] = assistant;
    },
    removeAssistant: function(name) {
        this.assistants[name] = undefined;
    },
    updateViews: function(list, success) {
        this.updateListModel(list, success);
        this.updateSummary(list, success);
        this.updateMarkets(list, success);
    },
    updateListModel: function(list, success) {
        if (list === this.watchList) {
            if (success) {
                this.watchListLastUpdateTime = (new Date()).getTime();
            }
            if (this.assistants.quoteList !== undefined) {
                this.assistants.quoteList.updateModelCallback(success);
            }
        }
    },
    updateSummary: function(list, success) {
        if (list === this.watchList || list === this.indexList || this.previewList) {
            if (this.assistants.summary !== undefined) {
                this.assistants.summary.updateDetails();
            }
        }
    },
    updateMarkets: function(list, success) {
        if (list === this.indexList && this.assistants.markets !== undefined) {
            this.assistants.markets.updateModelCallback(success);
        }
    },
    dbLoad: function() {
        var watchListData = localStorage.getItem('quoteList'),
            indexListData = localStorage.getItem('indexList');
        if (watchListData) {
            console.info('Watchlist data loaded.');
            try {
                watchListData = JSON.parse(watchListData);
                console.info('Watchlist data successfully parsed.');
                this.dbWatchListGetSuccess(watchListData);
            } catch (e) {
                console.error('JSON.parse error:', e.message);
            }
        } else { // watchlistData is null or ''
            this.dbWatchListUseDefault();
        }
        
        if (indexListData) {
            console.info('Index list data loaded.');
            try {
                indexListData = JSON.parse(indexListData);
                console.info('Index list data successfully parsed.');
                this.dbIndexListGetSuccess(indexListData);
            } catch (e) {
                console.error('JSON.parse error:', e.message);
            }
        } else {
            this.dbIndexListUseDefault();
        }
        
        // open the database
        // DEBUG - replace is true to recreate db every time; false for release
//        this.quotesDb = new Mojo.Depot(
//            {name: "quotesDb", version: 1, estimatedSize: 400000, replace: false},
//            this.dbLoadOk.bind(this),
//            function(result) {
//                console.warn("Can't open quotes database:", result);
//            }
//        );
    },
    // dbLoadOk() - callback for successful db request in setup;
    //     get stored db or fallback to using default quotes list
//    dbLoadOk: function() {
//        if (TopStocks.firstLaunch) {
//            this.dbIndexListUseDefault();
//        } else {
//            this.quotesDb.get("indexList", this.dbIndexListGetSuccess.bind(this),
//                this.dbIndexListUseDefault.bind(this));
//        }
//        this.quotesDb.get("newsFeed", this.dbNewsFeedGetSuccess.bind(this),
//            this.dbNewsFeedUseDefault.bind(this));
//    },
    // dbWatchListGetSuccess(ql) - successful retrieval of watch list object;
    //     call dbWatchListUseDefault if the watch list is empty or null
    //     or initiate an update to the watch list by calling updateQuotes
    dbWatchListGetSuccess: function(data) {
        if (data) {
            this.watchList = data;
            this.updateQuotes(this.watchList);
            if (this.assistants.quoteList !== undefined) {
                this.assistants.quoteList.updateListModel();
            }
        } else {
            this.dbWatchListUseDefault();
        }
    },
    // dbWatchListUseDefault() - callback for failed DB retrieval meaning
    //     no watch list, so use default
    dbWatchListUseDefault: function() {
        console.info("Database has no watch list. Will use default.");
        this.watchList = this.getDefaultList();
        this.updateQuotes(this.watchList);
    },
    // dbStoreWatchList() - writes contents of watch list array to quotes database depot
    dbStoreWatchList: function() {
        console.info("Saving watch list...");
        localStorage.setItem('quoteList', JSON.stringify(this.watchList));
    },
    dbIndexListGetSuccess: function(data) {
        if (data) {
            this.indexList = data;
            this.updateQuotes(this.indexList);
            if (this.assistants.markets !== undefined) {
                this.assistants.markets.updateListModel();
            }
        } else {
            this.dbIndexListUseDefault();
        }
    },
    dbIndexListUseDefault: function() {
        console.info("Database has no index list. Will use default.");
        this.updateQuotes(this.indexList);
    },
    dbStoreIndexList: function() {
        localStorage.setItem('indexList', JSON.stringify(this.indexList));
        console.info("Index list saved.");
    },
    dbNewsFeedGetSuccess: function(nf) {
        if (Object.toJSON(nf) == "{}" || nf === null) {
            this.dbNewsFeedUseDefault();
        } else {
            console.info("Retrieved news feed from DB");
            this.newsFeed = nf;

            // if update, then convert from older versions

            if (this.assistants.news !== undefined) {
                this.assistants.news.updateModelCallback();
            }
        }
    },
    dbNewsFeedUseDefault: function() {
        console.warn("Database has no news feed. Do nothing.");
    },
    dbStoreNewsFeed: function() {
        console.info("News feed save started");
//        this.quotesDb.add("newsFeed", this.newsFeed,
//            function() {console.info("News feed saved OK");},
//            this.dbStoreNewsFeedFailure);
    },
    dbStoreNewsFeedFailure: function(transaction, result) {
        console.warn("News feed database save error:", result);
    },
    getAuth: function(username, password, onSuccess, onFailure) {
        console.info("Getting Auth...");
        
        this._username = username;
        this._password = password;

//        new Ajax.Request(this.authUrl, {
//            method: "get",
//            parameters: {
//                service: "finance",
//                Email: this._username,
//                Passwd: this._password,
//                source: "company-app-1.0"
//            },
//            evalJSON: false,
//            "onSuccess": this.getAuthSuccess.bind(this, onSuccess),
//            "onFailure": this.getAuthFailure.bind(this, onFailure)
//        });
        
        xhr_get(this.authUrl,
            {
                service: "finance",
                Email: this._username,
                Passwd: this._password,
                source: "company-app-1.0"
            },
            this.getAuthSuccess.bind(this, onSuccess),
            this.getAuthFailure.bind(this, onFailure)
        );
    },
    getAuthSuccess: function(onSuccess, transport) {
        var lines = transport.responseText.split("\n"), i, pair, name, value;
        for (i = 0; i < lines.length; i++) {
            pair = lines[i].split("=");
            name = pair[0];
            value = pair[1];
            switch (name) {
//                case "SID":
//                    this._SID = value;
//                    break;
//                case "LSID":
//                    this._LSID = value;
//                    break;
                case "Auth":
                    this._Auth = value;
                    break;
            }
        }
        if (isFunction(onSuccess)) {
            onSuccess(transport);
        }
    },
    getAuthFailure: function(onFailure, transport) {
        console.info("Authorization failed");
        if (isFunction(onFailure)) {
            onFailure(transport);
        }
    },
    getAuthHeader: function(extra) {
        return Object.extend({Authorization: "GoogleLogin auth=" + this._Auth}, extra);
    },
    isAuth: function() {
        return this._Auth && !this._Auth.empty();
    },
    getPortfoliosUrl: function(portfolio, position, transaction) {
        // all portfolios
        var result = "http://finance.google.com/finance/feeds/default/portfolios";
        if (typeof portfolio == "string" && !portfolio.empty()) {
            // specific portfolio
            result += "/" + portfolio;
            if (typeof position == "string") {
                // all positions
                result += "/positions";
                if (!position.empty()) {
                    // specific position
                    result += "/" + position;
                    if (typeof transaction == "string") {
                        // all transactions
                        result += "/transactions";
                        if (!transaction.empty()) {
                            // specific transaction
                            result += "/" + transaction;
                        }
                    }
                }
            }
        }
        return result;
    },
    getPortfolios: function(onSuccess, onFailure) {
        console.info("Getting portfolios...");
//        new Ajax.Request(this.getPortfoliosUrl(), {
//            method: "get",
//            parameters: {
//                returns: true
//            },
//            requestHeaders: this.getAuthHeader(),
//            evalJSON: false,
//            "onSuccess": this.getPortfoliosSuccess.bind(this, onSuccess),
//            "onFailure": this.getPortfoliosFailure.bind(this, onFailure)
//        });
        xhr_get(this.getPortfoliosUrl(),
            {returns: true},
            this.getPortfoliosSuccess.bind(this, onSuccess),
            this.getPortfoliosFailure.bind(this, onFailure),
            this.getAuthHeader()
        );
    },
    getPortfoliosSuccess: function(onSuccess, transport) {
        var i, idStr, nodes, portfolioData, marketValue, gain, gainPercentage, daysGain,
            j, links, linkSelf, linkEdit,
            entries = transport.responseXML.getElementsByTagName("entry");
        this.portfolios.clear();
        for (i = 0; i < entries.length; i++) {
            marketValue = "";
            gain = "";
            daysGain = "";
            idStr = entries[i].getElementsByTagName("id")[0].textContent;
            portfolioData = entries[i].getElementsByTagName("portfolioData")[0];
            // getting market value
            nodes = portfolioData.getElementsByTagName("marketValue");
            if (nodes.length > 0) {
                nodes = nodes[0].getElementsByTagName("money");
                if (nodes.length > 0) {
                    marketValue = toCurrency(nodes[0].getAttribute("amount"),
                        nodes[0].getAttribute("currencyCode"));
                }
            }
            // getting gain
            nodes = portfolioData.getElementsByTagName("gain");
            if (nodes.length > 0) {
                nodes = nodes[0].getElementsByTagName("money");
                if (nodes.length > 0) {
                    gain = toCurrency(nodes[0].getAttribute("amount"));
                }
            }
            // getting gain percentage
            gainPercentage = (parseFloat(portfolioData.getAttribute("gainPercentage")) * 100).toFixed(2);
            // getting day's gain
            nodes = portfolioData.getElementsByTagName("daysGain");
            if (nodes.length > 0) {
                nodes = nodes[0].getElementsByTagName("money");
                if (nodes.length > 0) {
                    daysGain = toCurrency(nodes[0].getAttribute("amount"));
                }
            }
            // getting Self and Edit URLs
            links = entries[i].getElementsByTagName("link");
            j = 0;
            linkSelf = null;
            linkEdit = null;
            while (j < links.length && !(linkSelf && linkEdit)) {
                if (links[j].getAttribute("rel") == "self") {
                    linkSelf = links[j].getAttribute("href");
                } else if (links[j].getAttribute("rel") == "edit") {
                    linkEdit = links[j].getAttribute("href");
                }
                j++;
            }
            
            this.portfolios.push({
                id: idStr.substr(idStr.lastIndexOf("/") + 1),
                title: entries[i].getElementsByTagName("title")[0].textContent,
                "marketValue": marketValue,
                "gain": gain,
                "gainPercentage": gainPercentage,
                "daysGain": daysGain,
                "linkSelf": linkSelf,
                "linkEdit": linkEdit,
                positions: []
            });
        }
        if (isFunction(onSuccess)) {
            onSuccess(this.portfolios, transport);
        }
    },
    getPortfoliosFailure: function(onFailure, transport) {
        console.info("Getting portfolios failed");
        if (isFunction(onFailure)) {
            onFailure(this.portfolios, transport);
        }
    },
//    deletePortfolio: function(portfolioIndex, onSuccess, onFailure) {
//        console.info("Deleting portfolio", this.portfolios[portfolioIndex].title);
//        console.info("Using this URL:", this.portfolios[portfolioIndex].linkEdit);
//        new Ajax.Request(this.portfolios[portfolioIndex].linkEdit, {
//            method: "post",
//            requestHeaders: this.getAuthHeader({"X-HTTP-Method-Override": "DELETE"}),
//            evalJSON: false,
//            "onSuccess": this.deletePortfolioSuccess.bind(this, portfolioIndex,
//                onSuccess),
//            "onFailure": this.deletePortfolioFailure.bind(this, portfolioIndex,
//                onFailure)
//        });
//    },
//    deletePortfolioSuccess: function(portfolioIndex, onSuccess, transport) {
//        if (isFunction(onSuccess)) {
//            onSuccess(portfolioIndex, transport);
//        }
//    },
//    deletePortfolioFailure: function(portfolioIndex, onFailure, transport) {
//        if (isFunction(onFailure)) {
//            onFailure(portfolioIndex, transport);
//        }
//    },
    getPositions: function(portfolioIndex, onSuccess, onFailure) {
        console.info("Getting positions of the",
            this.portfolios[portfolioIndex].title, "portfolio");
        var positions = this.portfolios[portfolioIndex].positions;
//        new Ajax.Request(
//            this.getPortfoliosUrl(this.portfolios[portfolioIndex].id, ""),
//            {
//                method: "get",
//                requestHeaders: this.getAuthHeader(),
//                evalJSON: false,
//                "onSuccess": this.getPositionsSuccess.bind(this, positions,
//                    onSuccess),
//                "onFailure": this.getPositionsFailure.bind(this, positions,
//                    onFailure)
//            }
//        );
        xhr_get(this.getPortfoliosUrl(this.portfolios[portfolioIndex].id, ""), null,
            this.getPositionsSuccess.bind(this, positions, onSuccess),
            this.getPositionsFailure.bind(this, positions, onFailure),
            this.getAuthHeader()
        );
    },
    getPositionsSuccess: function(positions, onSuccess, transport) {
        var i, idStr, symbol,
            entries = transport.responseXML.getElementsByTagName("entry");
        positions.clear();
        for (i = 0; i < entries.length; i++) {
            idStr = entries[i].getElementsByTagName("id")[0].textContent;
            symbol = entries[i].getElementsByTagName("symbol")[0];
            positions.push({
                id: idStr.substr(idStr.lastIndexOf("/") + 1),
                title: entries[i].getElementsByTagName("title")[0].textContent,
                exchange: symbol.getAttribute("exchange"),
                "symbol": symbol.getAttribute("symbol"),
                transactions: []
            });
        }
        if (isFunction(onSuccess)) {
            onSuccess(positions, transport);
        }
    },
    getPositionsFailure: function(positions, onFailure, transport) {
        console.info("Get positions failed");
        if (isFunction(onFailure)) {
            onFailure(positions, transport);
        }
    },
    getPosition: function(portfolioIndex, positionIndex, onSuccess, onFailure) {
        var position = this.portfolios[portfolioIndex].positions[positionIndex];
//        new Ajax.Request(
//            this.getPortfoliosUrl(this.portfolios[portfolioIndex].id,
//                this.portfolios[portfolioIndex].positions[positionIndex].id),
//            {
//                method: "get",
//                requestHeaders: this.getAuthHeader(),
//                evalJSON: false,
//                "onSuccess": this.getPositionSuccess.bind(this, position, onSuccess),
//                "onFailure": this.getPositionFailure.bind(this, position, onFailure)
//            }
//        );
        xhr_get(this.getPortfoliosUrl(this.portfolios[portfolioIndex].id,
                    this.portfolios[portfolioIndex].positions[positionIndex].id),
            null,
            this.getPositionSuccess.bind(this, position, onSuccess),
            this.getPositionFailure.bind(this, position, onFailure),
            this.getAuthHeader()
        );
    },
    getPositionSuccess: function(position, onSuccess, transport) {
        var i, nodes, positionData, marketValue, gain, gainPercentage, daysGain, shares,
            entries = transport.responseXML.getElementsByTagName("entry");
        for (i = 0; i < entries.length; i++) {
            marketValue = "";
            gain = "";
            daysGain = "";
            positionData = entries[i].getElementsByTagName("positionData")[0];
            // getting market value (and multiply by 100 for UK stocks to fix a Google Finance bug)
            nodes = positionData.getElementsByTagName("marketValue");
            if (nodes.length > 0) {
                nodes = nodes[0].getElementsByTagName("money");
                if (nodes.length > 0) {
                    marketValue = toCurrency(position.exchange === "LON" ?
                        nodes[0].getAttribute("amount") * 100 :
                        nodes[0].getAttribute("amount"),
                        nodes[0].getAttribute("currencyCode"));
                }
            }
            // getting gain
            nodes = positionData.getElementsByTagName("gain");
            if (nodes.length > 0) {
                nodes = nodes[0].getElementsByTagName("money");
                if (nodes.length > 0) {
                    gain = toCurrency(nodes[0].getAttribute("amount"));
                }
            }
            // getting gain percentage
            gainPercentage = (parseFloat(positionData.getAttribute("gainPercentage")) * 100).toFixed(2);
            // getting day's gain
            nodes = positionData.getElementsByTagName("daysGain");
            if (nodes.length > 0) {
                nodes = nodes[0].getElementsByTagName("money");
                if (nodes.length > 0) {
                    daysGain = toCurrency(nodes[0].getAttribute("amount"));
                }
            }
            // getting shares
            shares = parseFloat(positionData.getAttribute("shares"));

            position.marketValue = marketValue;
            position.gain = gain;
            position.gainPercentage = gainPercentage;
            position.daysGain = daysGain;
            position.shares = shares;
        }
        if (isFunction(onSuccess)) {
            onSuccess(position, transport);
        }
    },
    getPositionFailure: function(position, onFailure, transport) {
        console.info("Get position failed");
        if (isFunction(onFailure)) {
            onFailure(position, transport);
        }
    },
    getPositionQuote: function(portfolioIndex, positionIndex, onSuccess, onFailure) {
        var position = this.portfolios[portfolioIndex].positions[positionIndex];
//        new Ajax.Request(this.quoteUrl, {
//            method: "get",
//            parameters: {
//                client: "ig",
//                q: position.exchange + ":" + position.symbol
//            },
//            evalJSON: false,
//            "onSuccess": this.getPositionQuoteSuccess.bind(this, position, onSuccess),
//            "onFailure": this.getPositionQuoteFailure.bind(this, position, onFailure)
//        });
        xhr_get(this.quoteUrl,
            {
                client: "ig",
                q: position.exchange + ":" + position.symbol
            },
            this.getPositionQuoteSuccess.bind(this, position, onSuccess),
            this.getPositionQuoteFailure.bind(this, position, onFailure)
        );
    },
    getPositionQuoteSuccess: function(position, onSuccess, transport) {
        var q, jsonStr = transport.responseText.substr(3),
            data = JSON.parse(jsonStr);
        if (data.length > 0) {
            q = data[0];
            position.last = q.l;
            position.changeText = q.c ? q.c + (q.cp ? " (" + q.cp + "%)" : "") : TopStocks.dummy;
            position.changeColor = q.ccol;
            if (isFunction(onSuccess)) {
                onSuccess(position, transport);
            }
        }
    },
    getPositionQuoteFailure: function(position, onFailure, transport) {
        console.info("Getting position's quote failed");
        if (isFunction(onFailure)) {
            onFailure(position, transport);
        }
    },
    getTransactions: function(portfolioIndex, positionIndex, onSuccess, onFailure) {
        console.info("Getting transations of the",
            this.portfolios[portfolioIndex].positions[positionIndex].title, "position in the",
            this.portfolios[portfolioIndex].title, "portfolio");
        var transactions = this.portfolios[portfolioIndex].positions[positionIndex].transactions;
//        new Ajax.Request(
//            this.getPortfoliosUrl(this.portfolios[portfolioIndex].id,
//                this.portfolios[portfolioIndex].positions[positionIndex].id, ""),
//            {
//                method: "get",
//                requestHeaders: this.getAuthHeader(),
//                evalJSON: false,
//                "onSuccess": this.getTransactionsSuccess.bind(this, transactions,
//                    onSuccess),
//                "onFailure": this.getTransactionsFailure.bind(this, transactions,
//                    onFailure)
//            }
//        );
        xhr_get(this.getPortfoliosUrl(this.portfolios[portfolioIndex].id,
                    this.portfolios[portfolioIndex].positions[positionIndex].id, ""),
            null,
            this.getTransactionsSuccess.bind(this, transactions, onSuccess),
            this.getTransactionsFailure.bind(this, transactions, onFailure),
            this.getAuthHeader()
        );
    },
    getTransactionsSuccess: function(transactions, onSuccess, transport) {
        var i, nodes, idStr,
            transactionData, type, shares, price, commission, date, notes,
            entries = transport.responseXML.getElementsByTagName("entry"),
            count = entries.length;
        transactions.clear();
        for (i = count - 1; i >= 0; i--) {
            price = "";
            commission = "";
            idStr = entries[i].getElementsByTagName("id")[0].textContent;
            transactionData = entries[i].getElementsByTagName("transactionData")[0];
            type = transactionData.getAttribute("type");
            shares = parseFloat(transactionData.getAttribute("shares"));
            date = transactionData.getAttribute("date");
            date = date ? date.substr(0, date.indexOf("T")).replace(/-/g, "/") : "";
            notes = transactionData.getAttribute("notes");
            // getting price
            nodes = transactionData.getElementsByTagName("price");
            if (nodes.length > 0) {
                nodes = nodes[0].getElementsByTagName("money");
                if (nodes.length > 0) {
                    price = toCurrency(nodes[0].getAttribute("amount"));
                }
            }
            // getting commission
            nodes = transactionData.getElementsByTagName("commission");
            if (nodes.length > 0) {
                nodes = nodes[0].getElementsByTagName("money");
                if (nodes.length > 0) {
                    commission = toCurrency(nodes[0].getAttribute("amount"));
                }
            }
            transactions.push({
                id: idStr.substr(idStr.lastIndexOf("/") + 1),
                title: entries[i].getElementsByTagName("title")[0].textContent,
                "type": $L(type),
                "shares": shares,
                "date": date,
                "notes": notes,
                "price": price,
                "commission": commission
            });
        }
        if (isFunction(onSuccess)) {
            onSuccess(transactions, transport);
        }
    },
    getTransactionsFailure: function(transactions, onFailure, transport) {
        if (isFunction(onFailure)) {
            onFailure(transactions, transport);
        }
    },
    checkAlerts: function(quote, now) {
        var i, n = quote.alerts.length, alert;
        for (i = 0; i < n; i++) {
            alert = quote.alerts[i];
            if (alert.status == 0 && this.isAlertConditionMet(quote, alert)) {
                if (now.getTime() > alert.date) {
                    alert.status = 2; // expired
                } else {
                    alert.status = 1; // triggered
                    alert.date = now.getTime();
                    TopStocks.showAlert({
                        "quote": quote,
                        "alert": alert,
                        "soundFile": alert.soundFile
                    });
                }
            }
        }
    },
    isAlertConditionMet: function(quote, alert) {
        var parameter = quote[alert.parameter];
        if (Object.isString(parameter)) {
            parameter = Number(parameter.replace(/,/g, ""));
        }
        if (!isNaN(parameter)) {
            switch (Number(alert.condition)) {
                case 0:
                    return parameter <= alert.value;
                case 1:
                    return parameter >= alert.value;
                case 2:
                    return parameter < alert.value;
                case 3:
                    return parameter > alert.value;
                default:
                    return false;
            }
        } else {
            return false;
        }
    },
    logOnServer: function(text, append) {
//        new Ajax.Request("http://topstocksapp.com/log/log.php",
//            {
//                method: "post",
//                parameters: {
//                    "text": text,
//                    "append": append
//                },
//                evalJSON: false
//            }
//        );
    }
};

Quotes.initialize();
